import React, { Component } from "react";
import {withRouter, Link } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import {
  CBadge,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg,
  CLink
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import axios from 'axios';


const validEmailRegex = RegExp(
  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
);
const validateForm = errors => {
  let valid = true;
  Object.values(errors).forEach(val => val.length > 0 && (valid = false));
  return valid;
};
export  default  class Login extends Component {

constructor(props){
      super(props);
        this.state = {
username: null,
password:null,
 errors: {
        username: '',
        password: ''

      }
};
this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

   }
   

 handleChange(event) {
console.log("event",event.target);
const target = event.target;
   
        const { name, value } = event.target;

    let errors = this.state.errors;

    switch (name) {
      case 'username': 
        errors.username = 
          value.length < 5
            ? 'User Name must be at least 5 characters long!'
            : '';
        break;
      case 'password': 
        errors.password = 
          value.length < 8
            ? 'Password must be at least 8 characters long!'
            : '';
        break;
      default:
        break;
    }

    this.setState({errors, [name]: value});

  }

  handleSubmit(event) {
event.preventDefault();
    //alert('A name was submitted: ' + this.state.value);
console.log(" this.state.value", this.state);
if(this.state.username!=null && this.state.password!=null ){
 if(validateForm(this.state.errors)) {
      console.info('Valid Form');
 const api = `http://35.193.187.77:9004/mla_server/api/v1/login`
axios.post(api,this.state,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'},
  })
        .then(res => {
if(res.data.statuscode==200){

 this.props.history.push('/dashboard'); // <--- The page you want to redirect your user to.

            console.log(res.data);
}else{
            console.log("inside else");
    let errors = this.state.errors;
 errors.username ='Invalid Username!';
 errors.password ='Invalid password!';
this.setState({errors});
}
   



       
    })
    }

}else{
      console.error('Invalid Form');
let errors = this.state.errors;
if(this.state.username==null || this.state.username==undefined ){
 errors.username ='Enter Username';
}
if(this.state.password==null || this.state.password==undefined ){
 errors.password ='Enter Password';
}
    }
 
  }
render() {
    const {errors} = this.state;
    
  return (
           <form onSubmit={this.handleSubmit}  noValidate >
    <div className="c-app c-default-layout flex-row align-items-center" style={
      {background: '#00acee52'}
    }>
      <CContainer>
        <CRow className="justify-content-center">
        <CCol md="4" style={{ padding: 0 }}>
        <CCard>
                <CCardBody>
                <CImg
            src={'images/logo.jpg'}
            className="c-avatar-img"
            style={{ height: 271}}
            alt="admin@bootstrapmaster.com"
          />
                  </CCardBody>
                  </CCard>
          </CCol>
          <CCol md="4"  style={{ padding: 0 }}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h2 class="color-skyblue">We The Leader</h2>
                    <p className="text-muted">Login to your account</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput type="text" placeholder="Username" autoComplete="username"  value={this.state.username}   name="username" onChange={this.handleChange} noValidate/>
 {errors.username.length > 0 && 
                <span className='error'>{errors.username}</span>}
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput type="password" placeholder="Password" autoComplete="current-password" value={this.state.password}   name="password" onChange={this.handleChange} noValidate/>
{errors.password.length > 0 && 
                <span className='error'>{errors.password}</span>}
                    </CInputGroup>
                    <CRow>
                      <CCol xs="6">

                        <CButton  type="submit" value="Submit" className="px-4 background-skyblue">Login</CButton>

                      </CCol>
                      {/* <CCol xs="6" className="text-right">
                        <CButton  className="px-0 color-skyblue">Forgot password?</CButton>
                      </CCol> */}
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
           
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
            </form>
  )
}
}


