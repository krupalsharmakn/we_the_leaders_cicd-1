import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
export  default class ListDepartment extends Component {
     constructor(props){
      super(props);
    this.state = {
      droplets: [],
      news_category:''
    }
   
       this.componentDidMount = this.componentDidMount.bind(this);
         this.filterevent = this.filterevent.bind(this);
    this.filtersubmit = this.filtersubmit.bind(this);

   }
 componentDidMount(){
   const api = `http://35.193.187.77:9004/mla_server/api/v1/list_news`
axios.get(api, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
            this.setState({ 'droplets': res.data.data });

       
    })

  }

 filterevent(event) {
console.log("event",event.target);
const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });

console.log("state",this.state);
  }
cancelclick(){
this.setState({
      news_category:''
       })
this.componentDidMount();
}

    filtersubmit(event) {
event.preventDefault();
var apidata={

}
    //alert('A name was submitted: ' + this.state.value);
  const api = `http://35.193.187.77:9004/mla_server/api/v1/list_news_by_category?news_category=`
axios.get(api+this.state.news_category,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
            this.setState({ 'droplets': res.data.data });
}else{
        this.setState({'droplets':'' })

}
       
    })
  }
   
viewdata(id){

    localStorage.setItem('newsid', id);

}
edit(id){
console.log("id",id);
    localStorage.setItem('newsid', id);

}
activatedata(id){

toastr.confirm('Are you sure confirm to activate the news?',{
 okText:     <button className="alertok" onClick={() => this.activate(id)} >Ok</button>,
  buttons: [{
    okText: 'Do not apply',
    className: 'do-not-apply-btn',
    handler: () => console.log('do-not-apply clicked')
  }, {
    cancel: true // move the cancel button to the end
  }]

});
console.log("ssdd");

}
deactivatedata(id){

toastr.confirm('Are you sure confirm to deactivate the news?',{
okText:     <button className="alertok" onClick={() => this.deactivate(id)} >Ok</button>,
  buttons: [{
    okText: 'Do not apply',
    className: 'do-not-apply-btn',
    handler: () => console.log('do-not-apply clicked')
  }, {
    cancel: true // move the cancel button to the end
  }]
  

});
console.log("ssdd");

}
activate(id){
console.log("ssdd",id);
const api = `http://35.193.187.77:9004/mla_server/api/v1/deactivate_news`
axios.post(api,{
 news_id: id,
    is_active:true
  }, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
this.props.history.push('/news/newslist');
    window.location.reload(); 
       
    })
}

deactivate(id){
console.log("ssdd",id);
const api = `http://35.193.187.77:9004/mla_server/api/v1/deactivate_news`
axios.post(api,{
 news_id: id,
    is_active:false
  }, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {

this.props.history.push('/news/newslist');
    window.location.reload(); 

    })
}
     render() {
        return (
           <div  >
<form onSubmit={this.filtersubmit}>
<div className="row">
<div className="form-group col-md-12" >
              <h1 id='title' className="floatleft">List of News</h1>
</div>
</div>
     <div className="form-group col-md-12">
          <h4> Filter News  </h4>
</div>
<div className="row">
                  <div className="form-group col-md-4">

<label>News category</label>
<select className="slectbox"  className="width" value={this.state.news_category}   name="news_category"  placeholder="Choose showFirstLastButtons" onChange={this.filterevent}
                    >
                    <option value="" disabled selected>Choose News Category </option>
                    <option value='Assembly'>Assembly </option>
                    <option value='Department'>Department</option>
                    <option value='Media'>Media </option>
                    <option value='Party'>Party </option>

                  </select>
</div>
</div>

        
        <button  type="submit" value="Submit" className="btn btn-primary">Submit</button>
              <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>
</form>
<Link to="/news/newsadd">
    <button className="btn btn-primary floatright">Add News</button>
</Link>
<div>
               <table className="table table-hover">
      <thead>
        <tr className="bgcolor">
          <th>Sl.No</th>
          <th>News Title</th>
          <th>Description</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        { (this.state.droplets.length > 0) ? this.state.droplets.map( (droplet, index) => {
           return (
            <tr key={ index }>
              <td>{ index+1 }</td>
              <td>{ (droplet.news_title.length>25)?(droplet.news_title.slice(0,23))+'..':(droplet.news_title) }</td>
              <td>{(droplet.news_description.length>25)?(droplet.news_description.slice(0,23))+'..':(droplet.news_description)}</td>
              <td><Link to="/news/newsview">
    <button className="btn btn-view" onClick={() => this.viewdata(droplet.news_id)}>View</button>
</Link>
{droplet.is_active ? (
<div >
    <button className="btn btn-green" onClick={() => this.deactivatedata(droplet.news_id)} >Deactivate</button>
</div>
) : <div >
    <button className="btn btn-green" onClick={() => this.activatedata(droplet.news_id)} >Activate</button>
</div>}
<Link to="/news/newsedit">
    <button className="btn btn-edit" onClick={() => this.edit(droplet.news_id)}>Edit</button>
</Link></td>
            </tr>
          )
         }) : <tr><td colSpan="5">No records found</td></tr> }
      </tbody>
    </table>
</div>
           </div>
        )
     }
}
