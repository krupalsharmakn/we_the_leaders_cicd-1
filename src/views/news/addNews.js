import React, { Component } from "react";
import { withRouter,Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
const validateForm = errors => {
  let valid = true;
  Object.values(errors).forEach(val => val.length > 0 && (valid = false));
  return valid;
};
export  default class AddDepartment extends Component {
  constructor(props){
    super(props);
     this.state = {
news_id:'',
news_title: null,
news_reported_on:null,
news_category:null,
news_description:'',
image:'',
errors: {
        news_title: '',
        news_reported_on: '',
news_category:''

      }

};
this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
this.fileChangedHandler = this.fileChangedHandler.bind(this);
  //  this.onChange = this.onChange.bind(this)
 }
cancelclick(){
 this.props.history.push('/news/newslist');
}

     handleFormReset = () => {
 this.state = {
news_title: '',
news_reported_on:'',
news_category:'',
news_description:'',};
    this.setState({
news_title: '',
news_reported_on:'',
news_category:'',
news_description:'',})

  }

fileChangedHandler = (event) => {
  const file = event.target.files[0];
    this.setState({'image':event.target.files[0]});
console.log("this.state",this.state);
console.log("file",file)

}
 handleChange(event) {
console.log("event",event.target);
const target = event.target;
   const { name, value } = event.target;

    let errors = this.state.errors;

    switch (name) {
      case 'news_title': 
        errors.news_title = 
          value.length < 1
            ? 'Enter news title'
            : '';
        break;
     case 'news_reported_on': 
  errors.news_reported_on = 
          value.length < 1
            ? 'Enter news reported on'
            : '';
        break;
     case 'news_category': 
  errors.news_category = 
          value.length < 1
            ? 'Select news category'
            : '';
        break;
      default:
        break;
    }

    this.setState({errors, [name]: value});


  }
 onChange(e){
    const re = /^[0-9\b]+$/;
    if (e.target.value === '' || re.test(e.target.value)) {
       this.setState({value: e.target.value})
    }
 }

  handleSubmit(event) {
event.preventDefault();

if(this.state.news_title!=null && this.state.news_reported_on!=null && this.state.news_category!=null ){
console.log(" this.state.deprtt");


 if(validateForm(this.state.errors)) {
console.log(" this.state.value", this.state);
  const api = `http://35.193.187.77:9004/mla_server/api/v1/add_news`
 const formData = new FormData();
formData.append("news_title",this.state.news_title);
formData.append("news_reported_on",this.state.news_reported_on);
formData.append("news_category",this.state.news_category);
formData.append("news_description",this.state.news_description);
formData.append("image",this.state.image);

axios.post(api,formData,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}  })
        .then(res => {
 this.props.history.push('/news/newslist');
toastr.success('News added successfuly')
            console.log(res.data);
  })
}
}else{

console.log("invalid form");

 let errors = this.state.errors;
if(this.state.news_title==null || this.state.news_title==undefined ){
 errors.news_title ='Enter news title';
}
if(this.state.news_reported_on==null || this.state.news_reported_on==undefined ){
 errors.news_reported_on ='Enter news reported on';
}
if(this.state.news_category==null || this.state.news_category==undefined ){
 errors.news_category ='Enter news category';
}



this.setState({errors});
}
    //alert('A name was submitted: ' + this.state.value);

  }
render() {
    const {errors} = this.state;
return (
         <form onSubmit={this.handleSubmit} onReset={this.handleFormReset} noValidate>
<div   className="row" >
              <div className="form-group col-md-6">
              <h3 className="floatleft" id='title'>Add News</h3>
</div>
              <div className="form-group col-md-6">
              <Link className="nav-link floatright" to={"/news/newslist"}>Back</Link>
</div>
</div>
<div   className="row" >
              <div className="form-group col-md-6">
                  <label>Title </label>
                  <input type="text" className="form-control" value={this.state.news_title}   name="news_title" maxlength="0" onChange={this.handleChange}  maxlength="50" noValidate />
{errors.news_title.length > 0 && 
                <span className='error'>{errors.news_title}</span>}
              </div>
              <div className="form-group col-md-6">
<label>Reported On </label> 

<input type="date" className="form-control"  value={this.state.news_reported_on}   name="news_reported_on" onChange={this.handleChange} noValidate />
{errors.news_reported_on.length > 0 && 
                <span className='error'>{errors.news_reported_on}</span>}
</div>

</div>
<div   className="row" >
              <div className="form-group  col-md-6">
                  <label> Description</label>
                  <textarea type="text" className="form-control"  value={this.state.news_description}   name="news_description" onChange={this.handleChange} maxlength="250"/>
              </div>
             < div className="form-group  col-md-6">
                  <label>              Select News category
:</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons"
                 value={this.state.news_category}   name="news_category" onChange={this.handleChange} noValidate >
                  <option value="" disabled selected>Select News category </option>
                  <option value="Assembly">Assembly</option>
                  <option value="Department"> Department</option>
                  <option value="Media">Media</option>
                  <option value="Party">Party</option>



                </select>
{errors.news_category.length > 0 && 
                <span className='error'>{errors.news_category}</span>}
          
              
</div>
</div>

<div  className="row">
<div className="form-group  col-md-6">
<input className="image"type="file" onChange={this.fileChangedHandler}  />

</div>
</div>

<button type="submit" className="btn btn-primary">Submit</button>
                          <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>
              </form>
)

}
}
