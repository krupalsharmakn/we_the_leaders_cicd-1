import React, { Component } from "react";
import { withRouter,Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
export  default class AddDepartment extends Component {
  constructor(props){
    super(props);
     this.state = {
news_id:'',
news_title: '',
news_reported_on:'',
news_category:'',
news_description:'',
image:''
};

this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
this.fileChangedHandler = this.fileChangedHandler.bind(this);
  //  this.onChange = this.onChange.bind(this)
 }

componentDidMount(){

    const workervalue = localStorage.getItem('newsid');
   const api = `http://35.193.187.77:9004/mla_server/api/v1/get_news?news_id=`
axios.get(api+workervalue, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
            this.setState({ news_id: res.data.data.news_id,
news_title: res.data.data.news_title,
news_reported_on:res.data.data.news_reported_on,
news_category:res.data.data.news_category,
news_description:res.data.data.news_description,
image:res.data.data.news_images});
       console.log("sdjhfhsdf",this.state);
       
    })
  }
cancelclick(){
 this.props.history.push('/news/newslist');
}

fileChangedHandler = (event) => {
  const file = event.target.files[0];
    this.setState({'image':event.target.files[0]});
console.log("this.state",this.state.image);
console.log("file",file)

}
     handleFormReset = () => {
 this.state = {
news_title: '',
news_reported_on:'',
news_category:'',
news_description:'',};
    this.setState({
news_title: '',
news_reported_on:'',
news_category:'',
news_description:'',})

  }

 handleChange(event) {
console.log("event",event.target);
const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });

  }
 onChange(e){
    const re = /^[0-9\b]+$/;
    if (e.target.value === '' || re.test(e.target.value)) {
       this.setState({value: e.target.value})
    }
 }

  handleSubmit(event) {
//event.preventDefault();
    //alert('A name was submitted: ' + this.state.value);
console.log(" this.state.value", this.state);
  const api = `http://35.193.187.77:9004/mla_server/api/v1/edit_news`
const formData = new FormData();
formData.append("news_id",this.state.news_id);
formData.append("news_title",this.state.news_title);
formData.append("news_reported_on",this.state.news_reported_on);
formData.append("news_category",this.state.news_category);
formData.append("news_description",this.state.news_description);
formData.append("image",this.state.image);
axios.post(api,formData,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}  })
        .then(res => {
 this.props.history.push('/news/newslist');
toastr.success('News updated successfuly')
            console.log(res.data);
  })
  }
render() {
return (
         <form onSubmit={this.handleSubmit} onReset={this.handleFormReset}>
<div   className="row" >
              <div className="form-group col-md-6">
              <h3 className="floatleft" id='title'>Edit News</h3>
</div>
              <div className="form-group col-md-6">
              <Link className="nav-link floatright" to={"/news/newslist"}>Back</Link>
</div>
</div>
<div   className="row" >
              <div className="form-group col-md-6">
                  <label>Title </label>
                  <input type="text" className="form-control" value={this.state.news_title}   name="news_title" onChange={this.handleChange} maxlength="50" />
              </div>
              <div className="form-group col-md-6">
<label>Reported On </label> 
<input type="date" className="form-control"  value={this.state.news_reported_on}   name="news_reported_on" onChange={this.handleChange} />

</div>

</div>
<div   className="row" >
              <div className="form-group  col-md-6">
                  <label> Description</label>
                  <textarea type="text" className="form-control"  value={this.state.news_description}   name="news_description" onChange={this.handleChange} maxlength="250" />
              </div>
             < div className="form-group  col-md-6">
                  <label>  Select News category:</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons"
                 value={this.state.news_category}   name="news_category" onChange={this.handleChange}  >
                  <option value="" disabled selected>Select News category </option>
                  <option value="Assembly">Assembly</option>
                  <option value="Department"> Department</option>
                  <option value="Media">Media</option>
                  <option value="Party">Party</option>



                </select>
          
              
</div>
</div>
<div  className="row">
<div className="form-group col-md-6">
 <img className="image"src={this.state.image} />
<input className="image"type="file" onChange={this.fileChangedHandler}  />

</div>
</div>


<button type="submit" className="btn btn-primary">Submit</button>
                          <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>
              </form>
)

}
}
