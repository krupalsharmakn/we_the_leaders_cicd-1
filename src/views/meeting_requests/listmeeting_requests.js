import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
export  default class ListMeetingRequest extends Component {
   constructor(props){
      super(props);
    this.state = {
      droplets: [],
meeting_start_date: '',
meeting_end_date:'',
    }
       this.componentDidMount = this.componentDidMount.bind(this);
  this.filterevent = this.filterevent.bind(this);
    this.filtersubmit = this.filtersubmit.bind(this);
   }
  


componentDidMount(){
       console.log("sdjhfhsdf");
   const api = `http://35.193.187.77:9004/mla_server/api/v1/list_meeting_requests`
axios.get(api, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
            this.setState({ 'droplets': res.data.data });  
    })

  }

cancelclick(){
this.setState({
     meeting_start_date: '',
meeting_end_date:'',
       })
this.componentDidMount();
}
filterevent(event) {
console.log("event",event.target);
const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });

console.log("state",this.state);
  }

    filtersubmit(event) {
event.preventDefault();
    //alert('A name was submitted: ' + this.state.value);
console.log(" this.state.value");
var apidata={
is_active: "true",
meeting_start_date: this.state.meeting_start_date,
meeting_end_date:  this.state.meeting_end_date,
}
  const api = `http://35.193.187.77:9004/mla_server/api/v1/meeting_filter`
axios.post(api,apidata,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
            this.setState({ 'droplets': res.data.data });
}else{
        this.setState({'droplets':'' })

}
       
    })
  }

activatedata(id){

toastr.confirm('Are you sure confirm to accept the meeting request?',{
 okText:     <button className="alertok" onClick={() => this.activate(id)} >Ok</button>,
  buttons: [{
    okText: 'Do not apply',
    className: 'do-not-apply-btn',
    handler: () => console.log('do-not-apply clicked')
  }, {
    cancel: true // move the cancel button to the end
  }]

});


}
deactivatedata(id){

toastr.confirm('Are you sure confirm to reject the meeting request?',{
okText:     <button className="alertok" onClick={() => this.deactivate(id)} >Ok</button>,
  buttons: [{
    okText: 'Do not apply',
    className: 'do-not-apply-btn',
    handler: () => console.log('do-not-apply clicked')
  }, {
    cancel: true // move the cancel button to the end
  }]
  

});


}

viewdata(id){
console.log("id",id);
    localStorage.setItem('meetingid', id);
}
   activate(id){
console.log("ssdd",id);
const api = `http://35.193.187.77:9004/mla_server/api/v1/deactivate_meeting_request`
axios.post(api,{
 meeting_request_id: id,
    is_active:true
  }, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
this.props.history.push('/meeting_requests/listmeeting_requests');
    window.location.reload();

       
    })
}


   deactivate(id){
console.log("ssdd",id);
const api = `http://35.193.187.77:9004/mla_server/api/v1/deactivate_meeting_request`
axios.post(api,{
 meeting_request_id: id,
    is_active:false
  }, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {

       this.props.history.push('/meeting_requests/listmeeting_requests');
    window.location.reload();

    })
}


     render() {
        return (
           <div  >
<form onSubmit={this.filtersubmit}>
<div className="row">
<div className="form-group col-md-12" >
              <h1 id='title' className="floatleft">List of Meeting Requests</h1>
</div>
</div>
     <div className="form-group col-md-12">
          <h4> Filter Meeting Requests  </h4>
</div>
<div className="row">
 <div className="form-group col-md-4">
<label>Meeting Scheduled From Date</label>
<input type="date" value={this.state.meeting_start_date} name="meeting_start_date"   className="width" onChange={this.filterevent} />
</div>

   <div className="form-group col-md-4">

<label>Meeting Scheduled To Date</label>
<input type="date" value={this.state.meeting_end_date} name="meeting_end_date"   className="width" onChange={this.filterevent} />
</div>
</div>

        
        <button  type="submit" value="Submit" className="btn btn-primary">Submit</button>
              <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>

</form>

{/* <Link to="/department/departmentadd">
    <button className="btn btn-primary floatright">Add Department</button>
</Link> */}
<div>
           <table className="table table-hover">
      <thead>
        <tr className="bgcolor">
          <th>Sl.No</th>
          <th>Subject</th>
          <th>Date</th>
          <th>Meeting reason</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        { (this.state.droplets.length > 0) ? this.state.droplets.map( (droplet, index) => {
           return (
            <tr key={ index }>
              <td>{ index+1 }</td>
              <td>{ droplet.subject }</td>
              <td>{ droplet.day_of_meeting}</td>
              <td>{ droplet.reason_for_meet }</td>
              <td><Link to="/meeting_requests/viewmeeting">
    <button className="btn btn-view" onClick={() => this.viewdata(droplet.meeting_request_id)}>View</button>
</Link>
{droplet.is_active ? (
<div >
    <button className="btn btn-green" onClick={() => this.deactivatedata(droplet.meeting_request_id)} >Reject</button>
</div>
) : <div >
    <button className="btn btn-green" onClick={() => this.activatedata(droplet.meeting_request_id)} >Accept</button>
</div>}


</td>
            </tr>
          )
         }) : <tr><td colSpan="5">Loading...</td></tr> }
      </tbody>
    </table>
</div>
           </div>
        )
     }
}
