import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
export  default class EditProfile extends Component {
constructor(){
      super();
        this.state = {
mla_name:'',
party_name: '',
constituency_name:'',
profile_description:'',
address:'',
mla_profile_id:'',
profile_image:''};
    //  this.onChange = this.onChange.bind(this);
this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

this.depttypeData = [
    { value: 'Road', name: 'Road' },
    { value: 'Transport', name: 'Transport' },
    { value: 'Garbage', name: 'Garbage' },
    { value: 'Water', name: 'Water' },
    { value: 'Education', name: 'Education' },
    { value: 'Healthcare', name: 'Healthcare' }                                                                
];


this.districtData = [
    { value: 'Shimoga', name: 'Shimoga' },
    { value: 'Bengalore', name: 'Bengalore' },
    { value: 'Belagavi', name: 'Belagavi' },
                                                                 
];
   }

componentDidMount(){


   const api = `http://35.193.187.77:9004/mla_server/api/v1/get_mla_profile?mla_profile_id=9357a6bc-3d48-49b0-9e79-daf0b864d930`
axios.get(api, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
            this.setState({
mla_name: res.data.data.mla_name,
party_name: res.data.data.party_name,
constituency_name:res.data.data.constituency_name,
profile_description:res.data.data.profile_description,
mla_profile_id:res.data.data.mla_profile_id,
profile_image:res.data.data.profile_image,
address:res.data.data.address,
 });

       
    })
  }
   
   onChange(e){
     const re = /^[0-9\b]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
        // this.setState({value: e.target.value})
      }
 }

     handleFormReset = () => {
    this.setState({
mla_name:'',
party_name: '',
constituency_name:'',
profile_description:'',
address:'',
mla_profile_id:''})
  }

 handleChange(event) {
console.log("event",event.target);
const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });

  }

  handleSubmit(event) {
event.preventDefault();
    //alert('A name was submitted: ' + this.state.value);
console.log(" this.state.value", this.state);
  const api = `http://35.193.187.77:9004/mla_server/api/v1/edit_mla_profile`
axios.post(api,this.state,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
this.props.history.push('/mlaprofile/profile');
toastr.success('Profile updated successfuly')       
    })
  }
render() {


return (
           <form onSubmit={this.handleSubmit} onReset={this.handleFormReset}>
<div   className="row" >
                <div className="form-group col-md-6">
                <h3 className="floatleft" id='title'>Edit Profile</h3>
</div>
                <div className="form-group col-md-6">
                <Link className="nav-link floatright" to={"/department/departmentlist"}>Back</Link>
</div>
</div>

                <div className="form-group  col-md-6">
<label>Name : </label>


<input className="form-control" value={this.state.mla_name}   name="mla_name" onChange={this.handleChange} maxlength="10"/>
               

</div>
                <div className="form-group col-md-6">
 <label>Party Name : </label>
<input className="form-control" value={this.state.party_name}   name="party_name" onChange={this.handleChange} maxlength="10"/>

</div>



                <div className="form-group  col-md-6">
                    <label> Constituency Name</label>
                    <input type="text" value={this.state.constituency_name} name="constituency_name" onChange={this.handleChange} className="form-control"  />
                </div>

      <div className="form-group  col-md-6">
                    <label> Address</label>
                    <input type="text" value={this.state.address} name="address" onChange={this.handleChange} className="form-control"  />
                </div>
  <div className="form-group  col-md-6 ">
                    <label>Profile Description</label>
<textarea type="text" className="pwidth" value={this.state.profile_description} name="profile_description" onChange={this.handleChange}  className="form-control "  />
                
                </div>
                



<div className="form-group">
                    <img className="profile " src={this.state.profile_image} />
                </div>





                <button type="submit" value="Submit" className="btn btn-primary">Submit</button>
                 
            </form>
)

}
}
