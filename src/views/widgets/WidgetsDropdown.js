import React, { Component } from "react";
import {
  CWidgetDropdown,
  CRow,
  CCol,
  CDropdown,
  CDropdownMenu,
  CDropdownItem,
  CDropdownToggle
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import ChartLineSimple from '../charts/ChartLineSimple'
import ChartBarSimple from '../charts/ChartBarSimple'
import axios from 'axios';



export  default class WidgetsDropdown extends Component {
constructor(props){
      super(props);
    this.state = {
      count: []
    }

   }

componentDidMount(){
       console.log("sdjhfhsdf");
   const api = `http://35.193.187.77:9004/mla_server/api/v1/dashboard_count`
axios.get(api,{ headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
            this.setState({ 'count': res.data.data });

       
    })

  }
  // render
     render() {
  return (
    <CRow>
     <CCol sm="6" lg="3">
       <CWidgetDropdown
 className="cardht"
        color="gradient-primary ts-3"
         header="Users"
         text={this.state.count.users_count}
         
        >
          
       </CWidgetDropdown>
      </CCol>

     <CCol sm="6" lg="3">
        <CWidgetDropdown
 className="cardht"
        color="gradient-info ts-3"
          header="Grievances"
          text={this.state.count.complaints_count}
          
       >
 
        </CWidgetDropdown>
      </CCol>

      <CCol sm="6" lg="3">
        <CWidgetDropdown
 
          color="gradient-warning"
          className="cardht ts-3"
          header="News"
          text={this.state.count.news_count}
        
        >
          
        </CWidgetDropdown>
      </CCol>

      <CCol sm="6" lg="3">
        <CWidgetDropdown
          className="cardht ts-3"
          color="gradient-danger"
          header="Events"
          text={this.state.count.events_count}
          
        >
        
        </CWidgetDropdown>
      </CCol>
    </CRow>
  )
}
}



