import React, { Component } from "react";
import { withRouter,Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
export  default class EditProject extends Component {
constructor(props){
      super(props);
        this.state = {
project_id:'',
project_title: '',
project_description:'',
project_start_date:'',
project_end_date:'',
project_status:'',
image:''};

  this.cancel = this.state

    //  this.onChange = this.onChange.bind(this);
this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
this.fileChangedHandler = this.fileChangedHandler.bind(this);
   }

componentDidMount(){
    const deptidvalue = localStorage.getItem('projectid');
 console.log("sdjhfhsdf",deptidvalue);
   const api = `http://35.193.187.77:9004/mla_server/api/v1/get_project?project_id=`
axios.get(api+deptidvalue, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
            this.setState({
project_id: res.data.data.project_id,
project_title: res.data.data.project_title,
project_description:res.data.data.project_description,
project_start_date:res.data.data.project_start_date,
project_end_date:res.data.data.project_end_date,
project_status:res.data.data.project_status,
image:res.data.data.project_images

 });

       
    })
  }
     handleFormReset = () => {
 this.state = {
project_title: '',
project_description:'',
project_start_date:'',
project_end_date:'',
project_status:''};
    this.setState( {
project_title: '',
project_description:'',
project_start_date:'',
project_end_date:'',
project_status:''})
  }
   onChange(e){
     const re = /^[0-9\b]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
        // this.setState({value: e.target.value})
      }
 }

 handleChange(event) {
console.log("event",event.target);
const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });

  }
cancelclick(){
 this.props.history.push('/projects/projectslist');
}
fileChangedHandler = (event) => {
  const file = event.target.files[0];
    this.setState({'image':event.target.files[0]});
console.log("this.state",this.state.image);
console.log("file",file)

}
  handleSubmit(event) {
event.preventDefault();

const formData = new FormData();
formData.append("project_id",this.state.project_id);
formData.append("project_title",this.state.project_title);
formData.append("project_description",this.state.project_description);
formData.append("project_start_date",this.state.project_start_date);
formData.append("project_end_date",this.state.project_end_date);
formData.append("project_status",this.state.project_status);
formData.append("image",this.state.image);
    //alert('A name was submitted: ' + this.state.value);
console.log(" this.state.value", this.state);
  const api = `http://35.193.187.77:9004/mla_server/api/v1/edit_project`
axios.post(api,formData,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}  })
        .then(res => {
 this.props.history.push('/projects/projectslist');
toastr.success('Project updated successfuly')
            console.log(res.data);



       
    })
  }
render() {
return (
         <form onSubmit={this.handleSubmit} onReset={this.handleFormReset}>
<div   className="row" >
              <div className="form-group col-md-6">
              <h3 className="floatleft" id='title'>Edit Project</h3>
</div>
              <div className="form-group col-md-6">
              <Link className="nav-link floatright" to={"/projects/projectslist"}>Back</Link>
</div>
</div>
<div   className="row" >
              <div className="form-group col-md-6">
                  <label>Project Title</label>
                  <input type="text" className="form-control" value={this.state.project_title}   name="project_title" onChange={this.handleChange} maxlength="50"/>
              </div>
              <div className="form-group col-md-6">
<label>Project Description</label>
<textarea className="form-control" value={this.state.project_description}  name="project_description" onChange ={this.handleChange} maxlength="200" />

</div>

</div>
<div   className="row" >
              <div className="form-group  col-md-6">
                  <label> Project start date</label>
                  <input type="date" className="form-control"  value={this.state.project_start_date}  name="project_start_date"  onChange = {this.handleChange} />
              </div>
<div className="form-group  col-md-6">
                  <label>Project end date</label>
                  <input type="date" className="form-control" value={this.state.project_end_date}  name="project_end_date"  onChange ={this.handleChange} />
              </div>
              
</div>


<div   className="row" >
              <div className="form-group  col-md-6">
                  <label> Project Status</label>

<select className="slectbox"  className="form-control width" placeholder="Select project status" value={this.state.project_status}  name="project_status"  onChange = {this.handleChange}
                    >
                    <option value="" disabled selected>Choose project status </option>
                    <option value="Completed"> Completed</option>
                    <option value="Ongoing">Ongoing </option>
                  </select>
              </div>

<div className="form-group col-md-6">
 <img className="image"src={this.state.image} />
<input className="image"type="file" onChange={this.fileChangedHandler}  />

</div>

</div>
              <button type="submit" value="Submit" className="btn btn-primary">Submit</button>
                           <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>
              
          </form>
)

}
}
