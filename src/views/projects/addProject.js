import React, { Component } from "react";
import {  withRouter,Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
const validateForm = errors => {
  let valid = true;
  Object.values(errors).forEach(val => val.length > 0 && (valid = false));
  return valid;
};
export  default class AddDepartment extends Component {
constructor(props){
      super(props);
        this.state = {
project_title: null,
project_description:'',
project_start_date:null,
project_end_date:null,
project_status:null,
image:'',
errors: {
        project_title: '',
        project_start_date: '',
project_end_date:'',
project_status:''

      }
};
  this.cancel = this.state

    //  this.onChange = this.onChange.bind(this);
this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
this.fileChangedHandler = this.fileChangedHandler.bind(this);
   }
     handleFormReset = () => {
 this.state = {
project_title: '',
project_description:'',
project_start_date:'',
project_end_date:'',
project_status:'',
};
    this.setState( {
project_title: '',
project_description:'',
project_start_date:'',
project_end_date:'',
project_status:''})
  }
   onChange(e){
     const re = /^[0-9\b]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
        // this.setState({value: e.target.value})
      }
 }

cancelclick(){
 this.props.history.push('/projects/projectslist');
}
  onChange(e){
      const re = /^[0-9\b]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
  this.handleChange(e)
         this.setState({value: e.target.value})
      }
   }
twoCalls = e => {

  this.onChange(e)
}


 handleChange(event) {
console.log("event",event.target);
const target = event.target;
   const { name, value } = event.target;

    let errors = this.state.errors;

    switch (name) {
      case 'project_title': 
        errors.project_title = 
          value.length < 1
            ? 'Enter project title'
            : '';
        break;
   
     case 'project_start_date': 
  errors.project_start_date = 
          value.length < 1
            ? 'Enter project start date'
            : '';
        break;
     case 'project_end_date': 
  errors.project_end_date = 
          value.length < 1
            ? 'Enter project end date'
            : '';
        break;
      default:
        break;
 case 'project_status': 
  errors.project_status = 
          value.length < 1
            ? 'Enter project status'
            : '';
        break;
    }

    this.setState({errors, [name]: value});


  }

fileChangedHandler = (event) => {
  const file = event.target.files[0];
    this.setState({'image':event.target.files[0]});
console.log("this.state",this.state);
console.log("file",file)

}

  handleSubmit(event) {
event.preventDefault();
if(this.state.project_title !=null && this.state.project_end_date!=null && this.state.project_start_date!=null){
console.log(" this.state.deprtt");

 if(validateForm(this.state.errors)) {

  const api = `http://35.193.187.77:9004/mla_server/api/v1/add_project`;

  const formData = new FormData();
formData.append("project_title",this.state.project_title);
formData.append("project_description",this.state.project_description);
formData.append("project_start_date",this.state.project_start_date);
formData.append("project_end_date",this.state.project_end_date);
formData.append("project_status",this.state.project_status);
formData.append("image",this.state.image);
axios.post(api,formData,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}  })
        .then(res => {
if(res.data.statuscode==200){
 this.props.history.push('/projects/projectslist');
toastr.success('Project added successfuly')
            console.log(res.data);
}
            console.log(res.data);



       
    })
}
}else{

console.log("invalid form");

 let errors = this.state.errors;
if(this.state.project_title==null || this.state.project_title==undefined ){
 errors.project_title ='Enter project title';
}
if(this.state.project_end_date==null || this.state.project_end_date==undefined ){
 errors.project_end_date ='Enter project end date';
}
if(this.state.project_start_date==null || this.state.project_start_date==undefined ){
 errors.project_start_date ='Enter project start date';
}
if(this.state.project_status==null || this.state.project_status==undefined ){
 errors.project_status ='Select project status';
}


this.setState({errors});
}
    //alert('A name was submitted: ' + this.state.value);
console.log(" this.state.value", this.state);

  }
render() {
    const {errors} = this.state;
return (
         <form onSubmit={this.handleSubmit} onReset={this.handleFormReset} noValidate>
<div   className="row" >
              <div className="form-group col-md-6">
              <h3 className="floatleft" id='title'>Add Project</h3>
</div>
              <div className="form-group col-md-6">
              <Link className="nav-link floatright" to={"/projects/projectslist"}>Back</Link>
</div>
</div>
<div   className="row" >
              <div className="form-group col-md-6">
                  <label>Project Title</label>
                  <input type="text" className="form-control" value={this.state.project_title}   name="project_title" onChange={this.handleChange} maxlength="50" noValidate />
{errors.project_title.length > 0 && 
                <span className='error'>{errors.project_title}</span>}
              </div>
              <div className="form-group col-md-6">
<label>Project Description</label>
<textarea className="form-control" value={this.state.project_description}  name="project_description" onChange ={this.handleChange} maxlength="200" />

</div>

</div>
<div   className="row" >
              <div className="form-group  col-md-6">
                  <label> Project start date</label>
                  <input type="date" className="form-control"  value={this.state.project_start_date}  name="project_start_date"  onChange = {this.handleChange} noValidate />
{errors.project_start_date.length > 0 && 
                <span className='error'>{errors.project_start_date}</span>}
              </div>
<div className="form-group  col-md-6">
                  <label>Project end date</label>
                  <input type="date" className="form-control" value={this.state.project_end_date}  name="project_end_date"  onChange ={this.handleChange} noValidate />
{errors.project_end_date.length > 0 && 
                <span className='error'>{errors.project_end_date}</span>}
              </div>
              
</div>


<div   className="row" >
              <div className="form-group  col-md-6">
                  <label> Project Status</label>

<select className="slectbox"  className="form-control width" placeholder="Select project status" value={this.state.project_status}  name="project_status"  onChange = {this.handleChange} noValidate
                    >
                    <option value="" disabled selected>Choose project status </option>
                    <option value="Completed"> Completed</option>
                    <option value="Ongoing">Ongoing </option>
                  </select >
{errors.project_status.length > 0 && 
                <span className='error'>{errors.project_status}</span>}
              </div>
              <div className="form-group  col-md-6">

<input type="file" className="image" onChange={this.fileChangedHandler}  />

</div>

</div>
              <button type="submit" value="Submit" className="btn btn-primary">Submit</button>
              <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>

              
          </form>
)

}
}
