import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
export  default class ListDepartment extends Component {
    constructor(props){
      super(props);
    this.state = {
      droplets: [],
      project_status:'',
project_start_date:'',
project_end_date:'',

    }
   
       this.componentDidMount = this.componentDidMount.bind(this);
       this.filterevent = this.filterevent.bind(this);
    this.filtersubmit = this.filtersubmit.bind(this);

   }
 componentDidMount(){
       console.log("sdjhfhsdf");
   const api = `http://35.193.187.77:9004/mla_server/api/v1/list_projects`
axios.get(api, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
            this.setState({ 'droplets': res.data.data });

       
    })

  }
cancelclick(){
this.setState({
  project_status:'',
project_start_date:'',
project_end_date:'',
       })
this.componentDidMount();
}
 filterevent(event) {
console.log("event",event.target);
const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });

console.log("state",this.state);
  }

    filtersubmit(event) {
event.preventDefault();
    //alert('A name was submitted: ' + this.state.value);
console.log(" this.state.value");
var apidata={
is_active: "true",
project_status: this.state.project_status,
project_start_date: this.state.project_start_date,
project_end_date: this.state.project_end_date

}
  const api = `http://35.193.187.77:9004/mla_server/api/v1/projects_filter`
axios.post(api,apidata,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
            this.setState({ 'droplets': res.data.data });
}else{
        this.setState({'droplets':'' })

}
       
    })
  }

viewdata(id){

    localStorage.setItem('projectid', id);

}
edit(id){
console.log("id",id);
    localStorage.setItem('projectid', id);

}
activatedata(id){

toastr.confirm('Are you sure confirm to activate the project?',{
 okText:     <button className="alertok" onClick={() => this.activate(id)} >Ok</button>,
  buttons: [{
    okText: 'Do not apply',
    className: 'do-not-apply-btn',
    handler: () => console.log('do-not-apply clicked')
  }, {
    cancel: true // move the cancel button to the end
  }]

});
console.log("ssdd");

}
deactivatedata(id){

toastr.confirm('Are you sure confirm to deactivate the project?',{
okText:     <button className="alertok" onClick={() => this.deactivate(id)} >Ok</button>,
  buttons: [{
    okText: 'Do not apply',
    className: 'do-not-apply-btn',
    handler: () => console.log('do-not-apply clicked')
  }, {
    cancel: true // move the cancel button to the end
  }]
  

});
console.log("ssdd");

}
activate(id){
console.log("ssdd",id);
const api = `http://35.193.187.77:9004/mla_server/api/v1/deactivate_project`
axios.post(api,{
 project_id: id,
    is_active:true
  }, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
this.props.history.push('/projects/projectslist');
              window.location.reload();
    })
}

deactivate(id){
console.log("ssdd",id);
const api = `http://35.193.187.77:9004/mla_server/api/v1/deactivate_project`
axios.post(api,{
 project_id: id,
    is_active:false
  }, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
this.props.history.push('/projects/projectslist');
       window.location.reload();

    })
}
     render() {
        return (
           <div  >
<form onSubmit={this.filtersubmit}>
<div className="row">
<div className="form-group col-md-12" >
              <h1 id='title' className="floatleft">List of Projects</h1>
</div>
</div>
     <div className="form-group col-md-12">
          <h4> Filter Projects  </h4>
</div>
<div className="row">
                  <div className="form-group col-md-4">

<label>Project status</label>
<select className="slectbox"  className="width" value={this.state.project_status}   name="project_status"  placeholder="Choose showFirstLastButtons" onChange={this.filterevent}
                    >
                    <option value="" disabled selected>ChooseProject Status </option>
                    <option value='Completed'>Completed Projects </option>
                    <option value='Ongoing'>Ongoin Projects</option>


                  </select>
</div>

   <div className="form-group col-md-4">

<label>Project Starts From</label>
<input type="date" value={this.state.project_start_date} name="project_start_date"  className="width" onChange={this.filterevent} />
</div>



<div className="form-group col-md-4">
<label>Project Ends From</label>
<input type="date" value={this.state.project_end_date} name="project_end_date"  className="width" onChange={this.filterevent} />
</div>
</div>
        
        <button  type="submit" value="Submit" className="btn btn-primary">Submit</button>
              <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>

</form>
<Link to="/projects/projectsadd">
    <button className="btn btn-primary floatright">Add Project</button>
</Link>
<div>
          <table className="table table-hover">
      <thead>
        <tr className="bgcolor">
          <th>SlNo</th>
          <th>Start Date</th>
          <th>End date</th>
          <th>Title</th>
          <th>Description</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        { (this.state.droplets.length > 0) ? this.state.droplets.map( (droplet, index) => {
           return (
            <tr key={ index }>
              <td>{ index+1 }</td>
              <td>{ droplet.project_start_date }</td>
              <td>{ droplet.project_end_date}</td>
              <td>{(droplet.project_title.length>20)?(droplet.project_title.slice(0,10))+'..':(droplet.project_title)}</td>
              <td>{(droplet.project_description.length>20)?(droplet.project_description.slice(0,20))+'..':(droplet.project_description)}</td>
              <td>{ droplet.project_status }</td>
              <td><Link to="/projects/projectsview">
    <button className="btn btn-view" onClick={() => this.viewdata(droplet.project_id)}>View</button>
</Link>

{droplet.is_active ? (
<div >
    <button className="btn btn-green" onClick={() => this.deactivatedata(droplet.project_id)} >Deactivate</button>
</div>
) : <div >
    <button className="btn btn-green" onClick={() => this.activatedata(droplet.project_id)} >Activate</button>
</div>}
<Link to="/projects/projectsedit">
    <button className="btn btn-edit" onClick={() => this.edit(droplet.project_id)}>Edit</button>
</Link></td>
            </tr>
          )
         }) : <tr><td colSpan="5">No records found</td></tr> }
      </tbody>
    </table>
</div>
           </div>
        )
     }
}
