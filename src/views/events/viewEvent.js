import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
export default class ViewDepartment extends Component {

 constructor(props){
      super(props);
    this.state = {
      events: []
    }

   }

componentDidMount(){
       console.log("sdjhfhsdf");
    const workervalue = localStorage.getItem('eventid');
   const api = `http://35.193.187.77:9004/mla_server/api/v1/get_event?event_id=`
axios.get(api+workervalue, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
            this.setState({ 'events': res.data.data });

       
    })
  }
    render() {
        return (
            <form>
                <div className="row" >
                    <div className="form-group col-md-6">
                        <h3 className="floatleft" id='title'>Event Details</h3>
                    </div>
                    <div className="form-group col-md-6">
                        <Link className="nav-link floatright" to={"/events/eventslist"}>Back</Link>
                    </div>
                </div>


                <div className="form-group">
                    <label className="floatleft padright" >Event Title :</label>
                   {this.state.events.event_title}

                </div>

                <div className="form-group">
                    <label className="floatleft padright">Event Description : </label>{this.state.events.event_description}


                </div>
                <div className="form-group">
                    <label className="floatleft padright">Event Start Date :

</label> {this.state.events.event_start_date}

                </div>

                <div className="form-group ">
                    <label className="floatleft padright">Event End Date : </label>{this.state.events.event_end_date}
                </div>
                <div className="form-group">
                    <label className="floatleft padright">Event Status :</label>{this.state.events.is_active} 

                </div>

                <div className="form-group">
                    <label className="floatleft padright">Contact Person Name : </label>{this.state.events.contact_person_name}

                </div>

                <div className="form-group">
                    <label className="floatleft padright">Contact Number : 

</label>{this.state.events.contact_person_num}

                </div>

                <div className="form-group">
                    <label className="floatleft padright">Taluk :</label>{this.state.events.taluk}
                </div>

                <div className="form-group">
                    <label className="floatleft padright">District :</label>{this.state.events.district}
                </div>
                <div className="form-group" >
 <img className="image" src={this.state.events.event_images} />
               
                </div>

            </form>
        )

    }
}


