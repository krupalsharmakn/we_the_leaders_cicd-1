import React, { Component } from "react";
import { withRouter,Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
const validateForm = errors => {
  let valid = true;
  Object.values(errors).forEach(val => val.length > 0 && (valid = false));
  return valid;
};
export  default class AddDepartment extends Component {
  constructor(props){
    super(props);
     this.state = {
event_title: null,
event_description:'',
event_start_date:null,
event_end_date:null,
contact_person_name:'',
contact_person_num:'',
taluk:'',
district:'',
district_list:[],
taluk_list:[],
ward_list:[],
image:'',
errors: {
        event_title: '',
        event_start_date: '',
event_end_date:'',
      }
};

this.getdistrict();
this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
   this.onChange = this.onChange.bind(this);
this.getdistrictdata = this.getdistrictdata.bind(this);
this.fileChangedHandler = this.fileChangedHandler.bind(this);
    this.loadPanchayat = this.loadPanchayat.bind(this);
    this.wardevent = this.wardevent.bind(this);
 }
 
 onChange(e){
    const re = /^[0-9\b]+$/;
    if (e.target.value === '' || re.test(e.target.value)) {
console.log("sdsdtrue");
  this.handleChange(e)
       this.setState({value: e.target.value})
    }
 }
cancelclick(){
 this.props.history.push('/events/eventslist');
}
twoCalls = e => {
console.log("twoCalls",e);
  this.onChange(e)
}

     handleFormReset = () => {
 this.state = {
event_title: '',
event_description:'',
event_start_date:'',
event_end_date:'',
contact_person_name:'',
contact_person_num:'',
taluk:'',
district:''};
    this.setState({
event_title: '',
event_description:'',
event_start_date:'',
event_end_date:'',
contact_person_name:'',
contact_person_num:'',
taluk:'',
district:''})

  }

fileChangedHandler = (event) => {
  const file = event.target.files[0];
    this.setState({'image':event.target.files[0]});
console.log("this.state",this.state);
console.log("file",file)

}
 handleChange(event) {
console.log("event",event.target);
const target = event.target;
   const { name, value } = event.target;

    let errors = this.state.errors;

    switch (name) {
      case 'event_title': 
        errors.event_title = 
          value.length < 1
            ? 'Enter event title'
            : '';
        break;
     case 'event_start_date': 
  errors.event_start_date = 
          value.event_start_date < 1
            ? 'Enter event start date'
            : '';
        break;
     case 'event_end_date': 
  errors.event_end_date = 
          value.length < 1
            ? 'Enter event end date'
            : '';
        break;
      default:
        break;
    }

    this.setState({errors, [name]: value});

  }

  handleSubmit(event) {
event.preventDefault();

if(this.state.event_title!=null && this.state.event_start_date!=null && this.state.event_end_date!=null ){
console.log(" this.state.deprtt");


 if(validateForm(this.state.errors)) {
 const formData = new FormData();
formData.append("event_title",this.state.event_title);
formData.append("event_description",this.state.event_description);
formData.append("event_start_date",this.state.event_start_date);
formData.append("event_end_date",this.state.event_end_date);
formData.append("contact_person_name",this.state.contact_person_name);
formData.append("contact_person_num",this.state.contact_person_num);
formData.append("taluk",this.state.taluk);
formData.append("district",this.state.district);
formData.append("image",this.state.image);


console.log(" this.state.value", this.state);
  const api = `http://35.193.187.77:9004/mla_server/api/v1/add_event`
axios.post(api,formData,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
 this.props.history.push('/events/eventslist');
toastr.success('Event added successfuly')
            console.log(res.data);
  })
}

}else{

console.log("invalid form");

 let errors = this.state.errors;
if(this.state.event_title==null || this.state.event_title==undefined ){
 errors.event_title ='Enter event title';
}
if(this.state.event_start_date==null || this.state.event_start_date==undefined ){
 errors.event_start_date ='Enter event start date';
}
if(this.state.event_end_date==null || this.state.event_end_date==undefined ){
 errors.event_end_date ='Enter event end date';
}



this.setState({errors});
}
    //alert('A name was submitted: ' + this.state.value);

  }

getdistrict(){
  const api = `http://35.193.187.77:9004/mla_server/api/v1/filter_category`
axios.get(api,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
        this.state.district_list = res.data.complaint_categories.district;
            console.log("this.district_list",this.state.district_list);

}else{


} 
 })

}
getdistrictdata(event){
            this.setState({ 'district': event.target.value});
	this.loadTaluk(event);

}
  loadTaluk(eventdata: any) {
    console.log("eventdata", eventdata.target.value);
console.log("this.district_list",this.state.district_list);
    var datalist = this.state.district_list.find(o => o.name == eventdata.target.value) || '';
    this.state.taluk_list = datalist.taluk;
    console.log("datalistssd", datalist)


    
  }

  loadPanchayat(eventdata: any){
            this.setState({ 'taluk': eventdata.target.value});
    var datalist = this.state.taluk_list.find(o => o.name == eventdata.target.value) || '';
    this.state.ward_list = datalist.panchayath_ward;
    console.log("this.state.ward_list", this.state.ward_list)
  }

wardevent(eventdata: any){
    console.log("eventdata", eventdata.target.value)
    var datalist = this.state.ward_list.find(o => o.name == eventdata.target.value) || '';
            this.setState({ 'panchayath_ward': eventdata.target.value});
            this.setState({ 'pin': datalist.pin});

}
render() {
    const {errors} = this.state;
return (
         <form onSubmit={this.handleSubmit} onReset={this.handleFormReset} noValidate>
<div   className="row" >
              <div className="form-group col-md-6">
              <h3 className="floatleft" id='title'>Add Events</h3>
</div>
              <div className="form-group col-md-6">
              <Link className="nav-link floatright" to={"/events/eventslist"}>Back</Link>
</div>
</div>
<div   className="row" >
              <div className="form-group col-md-6">
                  <label>Event Title</label>
                  <input type="text" className="form-control" value={this.state.event_title}   name="event_title" onChange={this.handleChange} maxlength="50" noValidate />
{errors.event_title.length > 0 && 
                <span className='error'>{errors.event_title}</span>}
              </div>
              <div className="form-group col-md-6">
<label>Event Description</label>
<textarea type="text" className="form-control"   value={this.state.event_description}   name="event_description" onChange={this.handleChange} maxlength="200"/>

</div>

</div>
<div   className="row" >
             <div className="form-group  col-md-6">
                  <label> Event start date</label>
<input type="date"  className="form-control"  value={this.state.event_start_date}   name="event_start_date" onChange={this.handleChange} noValidate />
{errors.event_start_date.length > 0 && 
                <span className='error'>{errors.event_start_date}</span>}

              </div>
<div className="form-group  col-md-6">
                   <label> Event end date</label>
<input type="date"  className="form-control"  value={this.state.event_end_date}   name="event_end_date" onChange={this.handleChange} noValidate  />
{errors.event_end_date.length > 0 && 
                <span className='error'>{errors.event_end_date}</span>}

              </div>
              
</div>


<div   className="row" >
              <div className="form-group  col-md-6">
                  <label> Contact person name</label>
<input type="text"  className="form-control"   value={this.state.contact_person_name}   name="contact_person_name" onChange={this.handleChange} maxlength="20" />

              </div>
<div className="form-group  col-md-6">
                  <label> Contact person number</label>
<input type="text"  className="form-control"   value={this.state.contact_person_num}   name="contact_person_num"     onChange={this.twoCalls} maxlength="10" />

              </div>

              
</div>


<div  className="row">
<div className="form-group  col-md-6">

<label>Select District</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons"  onChange={this.getdistrictdata} value={this.state.district}   name="district">
                    <option value="" disabled selected>Choose District </option>
                 {this.state.district_list.map(optn => (

                     <option value={optn.name} >{optn.name}</option>
                 ))}
             </select>
 
              </div>
              <div className="form-group  col-md-6">
          <label>Select Taluk</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons"  onChange={this.loadPanchayat}
  value={this.state.taluk}   name="taluk"                   >
                   <option value="" disabled selected>Choose Taluk </option>
                   {this.state.taluk_list.map(optn => (

                     <option value={optn.name}>{optn.name}</option>
                 ))}


                  </select>
</div>




</div>

<div  className="row">
<div className="form-group  col-md-6">
<input className="image"type="file" onChange={this.fileChangedHandler}  />

</div>
</div>

              <button type="submit" value="Submit" className="btn btn-primary">Submit</button>
                          <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>
          </form>
)

}
}
