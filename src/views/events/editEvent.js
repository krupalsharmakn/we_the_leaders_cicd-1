import React, { Component } from "react";
import {withRouter, Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
export  default class EditEvent extends Component {
  constructor(props){
    super(props);
     this.state = {
event_id:'',
event_title: '',
event_description:'',
event_start_date:'',
event_end_date:'',
contact_person_name:'',
contact_person_num:'',
taluk:'',
district:'',
district_list:[],
taluk_list:[],
ward_list:[],
image:''
};
this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
this.getdistrictdata = this.getdistrictdata.bind(this);
this.fileChangedHandler = this.fileChangedHandler.bind(this);
    this.loadPanchayat = this.loadPanchayat.bind(this);
    this.wardevent = this.wardevent.bind(this);

 }
 onChange(e){
    const re = /^[0-9\b]+$/;
    if (e.target.value === '' || re.test(e.target.value)) {
  this.handleChange(e)
       this.setState({value: e.target.value})
    }
 }
twoCalls = e => {

  this.onChange(e)
}

fileChangedHandler = (event) => {
  const file = event.target.files[0];
    this.setState({'image':event.target.files[0]});
console.log("this.state",this.state.image);
console.log("file",file)

}
cancelclick(){
 this.props.history.push('/events/eventslist');
}
componentDidMount(){
    const deptidvalue = localStorage.getItem('eventid');
 console.log("sdjhfhsdf",deptidvalue);
   const api = `http://35.193.187.77:9004/mla_server/api/v1/get_event?event_id=`
axios.get(api+deptidvalue, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
            this.setState({
event_id: res.data.data.event_id,
event_title: res.data.data.event_title,
event_description:res.data.data.event_description,
event_start_date:res.data.data.event_start_date,
event_end_date:res.data.data.event_end_date,
contact_person_name:res.data.data.contact_person_name,
contact_person_num:res.data.data.contact_person_num,
taluk:res.data.data.taluk,
district:res.data.data.district,
image:res.data.data.event_images
 });

       
    })
this.getdistrict();
  }
 
     handleFormReset = () => {
 this.state = {
event_title: '',
event_description:'',
event_start_date:'',
event_end_date:'',
contact_person_name:'',
contact_person_num:'',
taluk:'',
district:''};
    this.setState({
event_title: '',
event_description:'',
event_start_date:'',
event_end_date:'',
contact_person_name:'',
contact_person_num:'',
taluk:'',
district:''})

  }

 handleChange(event) {
console.log("event",event.target);
const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });

  }

  handleSubmit(event) {
event.preventDefault();
    //alert('A name was submitted: ' + this.state.value);
console.log(" this.state.value", this.state);
  const api = `http://35.193.187.77:9004/mla_server/api/v1/edit_event`
const formData = new FormData();
formData.append("event_id",this.state.event_id);
formData.append("event_title",this.state.event_title);
formData.append("event_description",this.state.event_description);
formData.append("event_start_date",this.state.event_start_date);
formData.append("event_end_date",this.state.event_end_date);
formData.append("contact_person_name",this.state.contact_person_name);
formData.append("contact_person_num",this.state.contact_person_num);
formData.append("taluk",this.state.taluk);
formData.append("district",this.state.district);
formData.append("image",this.state.image);
axios.post(api,formData,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
 this.props.history.push('/events/eventslist');
toastr.success('Event updated successfuly')
            console.log(res.data);
  })
  }

getdistrict(){
  const api = `http://35.193.187.77:9004/mla_server/api/v1/filter_category`
axios.get(api,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
        this.state.district_list = res.data.complaint_categories.district;
            console.log("this.district_list",this.state.district_list);

}else{


} 
 })

}
getdistrictdata(event){
            this.setState({ 'district': event.target.value});
	this.loadTaluk(event);

}
  loadTaluk(eventdata: any) {
    console.log("eventdata", eventdata.target.value);
console.log("this.district_list",this.state.district_list);
    var datalist = this.state.district_list.find(o => o.name == eventdata.target.value) || '';
    this.state.taluk_list = datalist.taluk;
    console.log("datalistssd", datalist)


    
  }

  loadPanchayat(eventdata: any){
            this.setState({ 'taluk': eventdata.target.value});
    var datalist = this.state.taluk_list.find(o => o.name == eventdata.target.value) || '';
    this.state.ward_list = datalist.panchayath_ward;
    console.log("this.state.ward_list", this.state.ward_list)
  }

wardevent(eventdata: any){
    console.log("eventdata", eventdata.target.value)
    var datalist = this.state.ward_list.find(o => o.name == eventdata.target.value) || '';
            this.setState({ 'panchayath_ward': eventdata.target.value});
            this.setState({ 'pin': datalist.pin});

}
render() {
return (
         <form onSubmit={this.handleSubmit} onReset={this.handleFormReset}>
<div   className="row" >
              <div className="form-group col-md-6">
              <h3 className="floatleft" id='title'>Edit Events</h3>
</div>
              <div className="form-group col-md-6">
              <Link className="nav-link floatright" to={"/events/eventslist"}>Back</Link>
</div>
</div>
<div   className="row" >
              <div className="form-group col-md-6">
                  <label>Event Title</label>
                  <input type="text" className="form-control" value={this.state.event_title}   name="event_title" onChange={this.handleChange} maxlength="50" />
              </div>
              <div className="form-group col-md-6">
<label>Event Description</label>
<textarea type="text" className="form-control"   value={this.state.event_description}   name="event_description" onChange={this.handleChange} maxlength="200" />

</div>

</div>
<div   className="row" >
             <div className="form-group  col-md-6">
                  <label> Event start date</label>
<input type="date"  className="form-control"  value={this.state.event_start_date}   name="event_start_date" onChange={this.handleChange} />

              </div>
<div className="form-group  col-md-6">
                   <label> Event end date</label>
<input type="date"  className="form-control"  value={this.state.event_end_date}   name="event_end_date" onChange={this.handleChange}  />

              </div>
              
</div>


<div   className="row" >
              <div className="form-group  col-md-6">
                  <label> Contact person name</label>
<input type="text"  className="form-control"   value={this.state.contact_person_name}   name="contact_person_name" onChange={this.handleChange} maxlength="20" />

              </div>
<div className="form-group  col-md-6">
                  <label> Contact person number</label>
<input type="text"  className="form-control"   value={this.state.contact_person_num}   name="contact_person_num" onChange={this.twoCalls} maxlength="10" />

              </div>

              
</div>


<div  className="row">
<div className="form-group  col-md-6">
<label>Select District</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons"  onChange={this.getdistrictdata} value={this.state.district}   name="district">
                    <option value="" disabled selected>Choose District </option>
                 {this.state.district_list.map( key => (

                     <option  key={key} value={key.name} >{key.name}</option>
                 ))}
             </select>
                  
              </div>
              <div className="form-group  col-md-6">
<label>Taluk:</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons"  onChange={this.loadPanchayat}
  value={this.state.taluk}   name="taluk"                   >
                   <option value="" disabled selected>Choose Taluk </option>
                   <option value={this.state.taluk} >{this.state.taluk} </option>
                   {this.state.taluk_list.map(optn => (

                     <option key={optn} value={optn.name}>{optn.name}</option>
                 ))}


                  </select>
</div>
</div>
<div  className="row">
<div className="form-group col-md-6">
 <img className="image"src={this.state.image} />
<input className="image"type="file" onChange={this.fileChangedHandler}  />

</div>
</div>


              <button type="submit" value="Submit" className="btn btn-primary">Submit</button>
                            <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>
          </form>
)

}
}
