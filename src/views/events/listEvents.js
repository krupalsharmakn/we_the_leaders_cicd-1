import React, { Component } from "react";
import { withRouter,Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
export  default class ListEvents extends Component {
   constructor(props){
      super(props);
    this.state = {
      droplets: [],
event_start_date:'',
event_end_date:''
    }

 this.filterevent = this.filterevent.bind(this);
    this.filtersubmit = this.filtersubmit.bind(this);

   }

   componentDidMount(){
       console.log("sdjhfhsdf");
   const api = `http://35.193.187.77:9004/mla_server/api/v1/list_events`
axios.get(api, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
            this.setState({ 'droplets': res.data.data });

       
    })

  }
cancelclick(){
this.setState({
     event_start_date:'',
event_end_date:''
       })
this.componentDidMount();
}

filterevent(event) {
console.log("event",event.target);
const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });

console.log("state",this.state);
  }

    filtersubmit(event) {
event.preventDefault();
    //alert('A name was submitted: ' + this.state.value);
console.log(" this.state.value");
var apidata={
event_start_date: this.state.event_start_date,
event_end_date: this.state.event_end_date
}
  const api = `http://35.193.187.77:9004/mla_server/api/v1/events_filter`
axios.post(api,apidata,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
            this.setState({ 'droplets': res.data.data });
}else{
        this.setState({'droplets':'' })

}
       
    })
  }
activatedata(id){

toastr.confirm('Are you sure confirm to activate the events?',{
 okText:     <button className="alertok" onClick={() => this.activate(id)} >Ok</button>,
  buttons: [{
    okText: 'Do not apply',
    className: 'do-not-apply-btn',
    handler: () => console.log('do-not-apply clicked')
  }, {
    cancel: true // move the cancel button to the end
  }]

});
console.log("ssdd");

}
deactivatedata(id){

toastr.confirm('Are you sure confirm to deactivate the events?',{
okText:     <button className="alertok" onClick={() => this.deactivate(id)} >Ok</button>,
  buttons: [{
    okText: 'Do not apply',
    className: 'do-not-apply-btn',
    handler: () => console.log('do-not-apply clicked')
  }, {
    cancel: true // move the cancel button to the end
  }]
  

});
console.log("ssdd");

}
activate(id){
console.log("ssdd",id);
const api = `http://35.193.187.77:9004/mla_server/api/v1/deactivate_event`
axios.post(api,{
 event_id: id,
    is_active:true
  }, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
 this.props.history.push('/events/eventslist');
       window.location.reload();
       
    })
}

deactivate(id){
console.log("ssdd",id);
const api = `http://35.193.187.77:9004/mla_server/api/v1/deactivate_event`
axios.post(api,{
 event_id: id,
    is_active:false
  }, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
 this.props.history.push('/events/eventslist');
              window.location.reload();
    })
}



edit(id){
console.log("id",id);
    localStorage.setItem('eventid', id);

}

viewdata(id){
console.log("id",id);
    localStorage.setItem('eventid', id);
}
     render() {
        return (
           <div  >

<form onSubmit={this.filtersubmit}>
<div className="row">
<div className="form-group col-md-12" >
              <h1 id='title' className="floatleft">List of Events</h1>
</div>
</div>
     <div className="form-group col-md-12">
          <h4> Filter Events  </h4>
</div>
<div className="row">
 <div className="form-group col-md-4">
<label>Event Scheduled From Date</label>
<input type="date" value={this.state.event_start_date} name="event_start_date"  className="width" onChange={this.filterevent} />
</div>

   <div className="form-group col-md-4">

<label>Event Scheduled To Date</label>
<input type="date" value={this.state.event_end_date} name="event_end_date"  className="width" onChange={this.filterevent} />
</div>
</div>

        
        <button  type="submit" value="Submit" className="btn btn-primary">Submit</button>
              <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>

</form>

<Link to="/events/eventsadd">
    <button className="btn btn-primary floatright">Add Event</button>
</Link>
<div>
              <table className="table table-hover">
      <thead>
        <tr className="bgcolor">
          <th>Sl.No</th>
          <th>Start Date</th>
          <th>End Date</th>
          <th>Title</th>
          <th>Description</th>
          <th>Event Status</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        { (this.state.droplets.length > 0) ? this.state.droplets.map( (droplet, index) => {
           return (
            <tr key={ index }>
              <td>{ index+1 }</td>
              <td>{ droplet.event_start_date }</td>
              <td>{ droplet.event_end_date}</td>
              <td>{ (droplet.event_title.length>20)?(droplet.event_title.slice(0,20))+'..':(droplet.event_title) }</td> 
              <td>{ (droplet.event_description.length>20)?(droplet.event_description.slice(0,20))+'..':(droplet.event_description) }</td>
              <td>{ droplet.is_active }</td>
              <td><Link to="/events/eventsview">
    <button className="btn btn-view" onClick={() => this.viewdata(droplet.event_id)}>View</button></Link>

{droplet.is_active ? (
<div >
    <button className="btn btn-green" onClick={() => this.deactivatedata(droplet.event_id)} >Deactivate</button>
</div>
) : <div >
    <button className="btn btn-green" onClick={() => this.activatedata(droplet.event_id)} >Activate</button>
</div>}

<Link to="/events/eventsedit">
    <button className="btn btn-edit" onClick={() => this.edit(droplet.event_id)}>Edit</button>
</Link>
</td>
            </tr>
          )
         }) : <tr><td colSpan="5">No records found</td></tr> }
      </tbody>
    </table>
</div>
           </div>
        )
     }
}
