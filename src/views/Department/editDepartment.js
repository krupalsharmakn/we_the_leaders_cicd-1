import React, { Component } from "react";
import {withRouter, Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
export  default class EditDepartment extends Component {
constructor(props){
      super(props);
        this.state = {
department_contact_id:'',
department: '',
email:'',
phone_number:'',
alternative_number:'',
address:'',
contact_person:'',
taluk:'',
district:'',
panchayath_ward:'',
pin:'',
district_list:[],
taluk_list:[],
ward_list:[],};
     this.onChange = this.onChange.bind(this);
this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
this.getdistrictdata = this.getdistrictdata.bind(this);
    this.loadPanchayat = this.loadPanchayat.bind(this);
    this.wardevent = this.wardevent.bind(this);

this.depttypeData = [
    { value: 'Road', name: 'Road' },
    { value: 'Transport', name: 'Transport' },
    { value: 'Garbage', name: 'Garbage' },
    { value: 'Water', name: 'Water' },
    { value: 'Education', name: 'Education' },
    { value: 'Healthcare', name: 'Healthcare' }                                                                
];


this.districtData = [
    { value: 'Shimoga', name: 'Shimoga' },
    { value: 'Bengalore', name: 'Bengalore' },
    { value: 'Belagavi', name: 'Belagavi' },
                                                                 
];
   }

componentDidMount(){
    const deptidvalue = localStorage.getItem('deptid');
 console.log("sdjhfhsdf",deptidvalue);
   const api = `http://34.93.47.253:9004/mla_server/api/v1/get_department_contact?department_contact_id=`
axios.get(api+deptidvalue, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
            this.setState({
department_contact_id: res.data.data.department_contact_id,
department: res.data.data.department,
email:res.data.data.email,
phone_number:res.data.data.phone_number,
alternative_number:res.data.data.alternative_number,
address:res.data.data.address,
contact_person:res.data.data.contact_person,
taluk:res.data.data.taluk,
district:res.data.data.district,
panchayath_ward:res.data.data.panchayath_ward,
pin:res.data.data.pin,
 });

       
    })
this.getdistrict();
  }
getdistrict(){
  const api = `http://34.93.47.253:9004/mla_server/api/v1/filter_category`
axios.get(api,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
        this.state.district_list = res.data.complaint_categories.district;
            console.log("this.district_list",this.state.district_list);

}else{


} 
 })

}
getdistrictdata(event){
            this.setState({ 'district': event.target.value});
	this.loadTaluk(event);

}
  loadTaluk(eventdata: any) {
    console.log("eventdata", eventdata.target.value);
console.log("this.district_list",this.state.district_list);
    var datalist = this.state.district_list.find(o => o.name == eventdata.target.value) || '';
    this.state.taluk_list = datalist.taluk;
    console.log("datalistssd", datalist)


    
  }

  loadPanchayat(eventdata: any){
            this.setState({ 'taluk': eventdata.target.value});
    var datalist = this.state.taluk_list.find(o => o.name == eventdata.target.value) || '';
    this.state.ward_list = datalist.panchayath_ward;
    console.log("this.state.ward_list", this.state.ward_list)
  }

wardevent(eventdata: any){
    console.log("eventdata", eventdata.target.value)
    var datalist = this.state.ward_list.find(o => o.name == eventdata.target.value) || '';
            this.setState({ 'panchayath_ward': eventdata.target.value});
            this.setState({ 'pin': datalist.pin});

}
   
   onChange(e){
console.log("sdsddonchange");
     const re = /^[0-9\b]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
  this.handleChange(e)
        this.setState({value: e.target.value})
      }
 }

twoCalls = e => {

  this.onChange(e)
}
cancelclick(){
 this.props.history.push('/department/departmentlist');
}
     handleFormReset = () => {
    this.setState({
department: '',
email:'',
phone_number:'',
alternative_number:'',
address:'',
contact_person:'',
taluk:'',
district:'',
panchayath_ward:'',
pin:''})
  }

 handleChange(event) {
console.log("event",event.target);
const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });

  }

  handleSubmit(event) {
event.preventDefault();
    //alert('A name was submitted: ' + this.state.value);
console.log(" this.state.value", this.state);
  const api = `http://34.93.47.253:9004/mla_server/api/v1/edit_department_contact`
axios.post(api,this.state,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}  })
        .then(res => {
 this.props.history.push('/department/departmentlist');
toastr.success('Department updated successfuly')
            console.log(res.data);



       
    })
  }
render() {


return (
           <form onSubmit={this.handleSubmit} onReset={this.handleFormReset}>
<div   className="row" >
                <div className="form-group col-md-6">
                <h3 className="floatleft" id='title'>Edit Department</h3>
</div>
                <div className="form-group col-md-6">
                <Link className="nav-link floatright" to={"/department/departmentlist"}>Back</Link>
</div>
</div>
<div   className="row" >
                <div className="form-group  col-md-6">
<label>Select Department Type :</label>


<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons" name="department" value={this.state.department}  onChange={this.handleChange}  >
    {this.depttypeData.map((key) => {
                  return   <option  key={key} value={key.name} >{key.name}</option>;
    })}
</select>
               

</div>
                <div className="form-group col-md-6">
 <label>Phone number</label>
<input className="form-control" value={this.state.phone_number}   name="phone_number" onChange={this.twoCalls} maxlength="10"/>

</div>

</div>
<div   className="row" >
                <div className="form-group  col-md-6">
                    <label> Alternative number</label>
                    <input type="text" value={this.state.alternative_number} name="alternative_number" onChange={this.twoCalls} className="form-control"  />
                </div>
  <div className="form-group  col-md-6">
                    <label>Email Id</label>
                    <input type="email" value={this.state.email} name="email" onChange={this.handleChange}  className="form-control" />
                </div>
                
</div>


<div   className="row" >
                <div className="form-group  col-md-6">
                    <label> Contact Person</label>
<input type="text" value={this.state.contact_person}  name="contact_person" onChange={this.handleChange}  className="form-control"  />

                </div>
  <div className="form-group  col-md-6">
                    <label>Address</label>
<textarea type="text" value={this.state.address} name="address" onChange={this.handleChange}  className="form-control"  />
                </div>
                
</div>


<div  className="row">
                <div className="form-group  col-md-6">

<label>Select District</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons"  onChange={this.getdistrictdata} value={this.state.district}   name="district">
                    <option value="" disabled selected>Choose District </option>
                 {this.state.district_list.map( key => (

                     <option  key={key} value={key.name} >{key.name}</option>
                 ))}
             </select>


</div>

 <div className="form-group  col-md-6">
<label>Select Taluk</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons"  onChange={this.loadPanchayat}
  value={this.state.taluk}   name="taluk"                   >
                   <option value="" disabled selected>Choose Taluk </option>
                   <option value={this.state.taluk} >{this.state.taluk} </option>
                   {this.state.taluk_list.map(optn => (

                     <option key={optn} value={optn.name}>{optn.name}</option>
                 ))}


                  </select>
</div>

</div>

<div  className="row">
                <div className="form-group  col-md-6">
<label>Select Ward</label>
<select className="slectbox"  className="form-control width"  placeholder="Choose showFirstLastButtons" onChange={this.wardevent}
  value={this.state.panchayath_ward}   name="panchayath_ward"        
                    >
                   <option value="" disabled selected>Choose Ward </option>
                   <option value={this.state.panchayath_ward} >{this.state.panchayath_ward} </option>
                   {this.state.ward_list.map(optn => (

                     <option>{optn.name}</option>
                 ))}
                  </select>
</div>

 <div className="form-group  col-md-6">
<label>PIN code</label>
  <input type="text" value={this.state.pin} onChange={this.handleChange} name="pin" className="form-control" />
</div>

</div>


                <button type="submit" value="Submit" className="btn btn-primary">Submit</button>
               <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>
                 
            </form>
)

}
}
