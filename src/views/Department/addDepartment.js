import React, { Component } from "react";
import { withRouter,Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
const validEmailRegex = RegExp(
  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
);
const validateForm = errors => {
  let valid = true;
  Object.values(errors).forEach(val => val.length > 0 && (valid = false));
  return valid;
};
export  default class AddDepartment extends Component {
constructor(props){
      super(props);
        this.state = {
department: null,
email:null,
phone_number:null,
alternative_number:'',
address:'',
contact_person:'',
taluk:'',
district:null,
panchayath_ward:'',
pin:'',
district_list:[],
district:'',
taluk:'',
taluk_list:[],
ward_list:[],
errors: {
        department: '',
        email: '',
phone_number:'',
district:''

      }
};

this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
      this.onChange = this.onChange.bind(this);
this.getdistrictdata = this.getdistrictdata.bind(this);
    this.loadPanchayat = this.loadPanchayat.bind(this);
    this.wardevent = this.wardevent.bind(this);

   }
   
componentDidMount(){
this.getdistrict();
}
   onChange(e){
console.log("sdsddonchange");
     const re = /^[0-9\b]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
  this.handleChange(e)
        this.setState({value: e.target.value})
      }
 }

twoCalls = e => {

  this.onChange(e)
}
     handleFormReset = () => {
    this.setState({
department: '',
email:'',
phone_number:'',
alternative_number:'',
address:'',
contact_person:'',
taluk:'',
district:'',
panchayath_ward:'',
pin:''})
  }

 handleChange(event) {
console.log("event",event.target);
const target = event.target;
 const { name, value } = event.target;

    let errors = this.state.errors;

    switch (name) {
      case 'department': 
        errors.department = 
          value.length < 1
            ? 'Enter department name!'
            : '';
        break;
     case 'email': 
        errors.email = 
          validEmailRegex.test(value)
            ? ''
            : 'Email is not valid!';
        break;
     case 'phone_number': 
  errors.phone_number = 
          value.phone_number < 10
            ? 'Enter valid phone number!'
            : '';
        break;
     case 'district': 
  errors.district = 
          value.length < 1
            ? 'Select district name!'
            : '';
        break;
      default:
        break;
    }

    this.setState({errors, [name]: value});

  }
cancelclick(){
 this.props.history.push('/department/departmentlist');
}
  handleSubmit(event) {
event.preventDefault();
   //alert('A name was submitted: ' + this.state.value);
console.log(" this.state.value", this.state.department);

if(this.state.department!=null && this.state.email!=null && this.state.phone_number!=null  && this.state.district!=null ){
console.log(" this.state.deprtt", this.state.errors.department);


 if(validateForm(this.state.errors)) {
  const api = `http://34.93.47.253:9004/mla_server/api/v1/add_department_contact`
axios.post(api,this.state,
  { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} }
  )
        .then(res => {
if(res.data.statuscode==200){
 this.props.history.push('/department/departmentlist');
toastr.success('Department added successfuly')
            console.log(res.data);
}
            console.log(res.data);
    })
}

}else{

console.log("invalid form",this.state.deparment);

 let errors = this.state.errors;
if(this.state.department==null || this.state.department==undefined ){
 errors.department ='Select department';
}
if(this.state.email==null || this.state.email==undefined ){
 errors.email ='Enter email id';
}
if(this.state.phone_number==null || this.state.phone_number==undefined ){
 errors.phone_number ='Enter phone number';
}
if(this.state.district==null || this.state.district==undefined ){
 errors.district ='Select district';
}


this.setState({errors});
}




  }

getdistrict(){
  const api = `http://34.93.47.253:9004/mla_server/api/v1/filter_category`
axios.get(api,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
        this.state.district_list = res.data.complaint_categories.district;
            console.log("this.district_list",this.state.district_list);

}else{


} 
 })

}
getdistrictdata(event){
            this.setState({ 'district': event.target.value});
	this.loadTaluk(event);

}
  loadTaluk(eventdata: any) {
    console.log("eventdata", eventdata.target.value);
console.log("this.district_list",this.state.district_list);
    var datalist = this.state.district_list.find(o => o.name == eventdata.target.value) || '';
    this.state.taluk_list = datalist.taluk;
    console.log("datalistssd", datalist)


    
  }

  loadPanchayat(eventdata: any){
            this.setState({ 'taluk': eventdata.target.value});
    var datalist = this.state.taluk_list.find(o => o.name == eventdata.target.value) || '';
    this.state.ward_list = datalist.panchayath_ward;
    console.log("this.state.ward_list", this.state.ward_list)
  }

wardevent(eventdata: any){
    console.log("eventdata", eventdata.target.value)
    var datalist = this.state.ward_list.find(o => o.name == eventdata.target.value) || '';
            this.setState({ 'panchayath_ward': eventdata.target.value});
            this.setState({ 'pin': datalist.pin});

}
render() {

    const {errors} = this.state;
return (
           <form onSubmit={this.handleSubmit} onReset={this.handleFormReset} noValidate>
<div   className="row" >
                <div className="form-group col-md-6">
                <h3 className="floatleft" id='title'>Add Department</h3>
</div>
                <div className="form-group col-md-6">
                <Link className="nav-link floatright" to={"/department/departmentlist"}>Back</Link>
</div>
</div>
<div   className="row" >
                <div className="form-group  col-md-6">
<label>Select Department Type :</label>
<select className="slectbox"  className="form-control width" placeholder="Select Department Type" value={this.state.department}   name="department" onChange={this.handleChange}
                 noValidate   >

                    <option value="" disabled selected>Choose Department </option>
                    <option value="Road"> Road</option>
                    <option value="Transport">Transport </option>
                    <option value="Garbage">Garbage </option>


                  </select>
 {errors.department.length > 0 && 
                <span className='error'>{errors.department}</span>}
</div>
                <div className="form-group col-md-6">
 <label>Phone Number</label>
<input className="form-control" value={this.state.phone_number}   name="phone_number"  onChange={this.twoCalls} maxlength="10" noValidate/>
 {errors.phone_number.length > 0 && 
                <span className='error'>{errors.phone_number}</span>}

</div>

</div>
<div   className="row" >
                <div className="form-group  col-md-6">
                    <label> Alternative Number</label>
                    <input type="text" value={this.state.alternative_number} name="alternative_number"onChange={this.twoCalls} maxlength="10"  className="form-control"  />
                </div>
  <div className="form-group  col-md-6">
                    <label>Email Id</label>
                    <input type="email" value={this.state.email} name="email" onChange={this.handleChange}  className="form-control" noValidate />
{errors.email.length > 0 && 
                <span className='error'>{errors.email}</span>}

                </div>
                
</div>


<div   className="row" >
                <div className="form-group  col-md-6">
                    <label> Contact Person</label>
<input type="text" value={this.state.contact_person}  name="contact_person" onChange={this.handleChange}  className="form-control" maxlength="12"   />

                </div>
  <div className="form-group  col-md-6">
                    <label>Address</label>
<textarea type="text" value={this.state.address} name="address" onChange={this.handleChange}  className="form-control" maxlength="150"  />
                </div>
                
</div>


<div  className="row">
                 <div className="form-group col-md-6">

<label>Select District</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons"  onChange={this.getdistrictdata} value={this.state.district}   name="district">
                    <option value="" disabled selected>Choose District </option>
                 {this.state.district_list.map(optn => (

                     <option value={optn.name}>{optn.name}</option>
                 ))}
             </select>

{errors.district.length > 0 && 
                <span className='error'>{errors.district}</span>}
</div>

 <div className="form-group col-md-6">

<label>Select Taluk</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons"  onChange={this.loadPanchayat}
  value={this.state.taluk}   name="taluk"                   >
                   <option value="" disabled selected>Choose Taluk </option>
                   {this.state.taluk_list.map(optn => (

                     <option value={optn.name}>{optn.name}</option>
                 ))}


                  </select>
</div>


</div>

<div  className="row">
            <div className="form-group col-md-6">
<label>Select Ward</label>
<select className="slectbox"  className="form-control width"  placeholder="Choose showFirstLastButtons" onChange={this.wardevent}
  value={this.state.panchayath_ward}   name="panchayath_ward"        
                    >
                   <option value="" disabled selected>Choose Ward </option>
                   {this.state.ward_list.map(optn => (

                     <option value={optn.name}>{optn.name}</option>
                 ))}
                  </select>
</div>

 <div className="form-group  col-md-6">
<label>PIN code</label>
  <input type="text" value={this.state.pin} onChange={this.handleChange} name="pin" className="form-control"  maxlength="06"/>
</div>

</div>


                <button type="submit" value="Submit" className="btn btn-primary">Submit</button>
               <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>
            </form>
)

}
}
