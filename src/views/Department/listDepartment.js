import React, { Component } from "react";
import { withRouter,Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
export  default class ListDepartment extends Component {
    constructor(props){
      super(props);
    this.state = {
      droplets:'',
department_type:'',
district:'',
taluk:'',
ward_no:'',
pincode:'',
district_list:[],
districtdata:'',
talukdata:'',
warddata:'',
taluk_list:[],
ward_list:[]
    }
  
       this.componentDidMount = this.componentDidMount.bind(this);
          this.viewdata = this.viewdata.bind(this);
 this.filterevent = this.filterevent.bind(this);
    this.filtersubmit = this.filtersubmit.bind(this);
          this.activate = this.activate.bind(this);
this.getdistrictdata = this.getdistrictdata.bind(this);
    this.loadPanchayat = this.loadPanchayat.bind(this);
    this.wardevent = this.wardevent.bind(this);
    this.listdepttype = this.listdepttype.bind(this);

   }


componentDidMount(){
       console.log("sdjhfhsdf");
   const api = `http://34.93.47.253:9004/mla_server/api/v1/list_all_departmentcontact`
axios.get(api, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
this.listdept=res.data.data;
            console.log(res.data.data);
console.log("this.listdeptlist",this.listdept.length);
            this.setState({ 'droplets': res.data.data });

       
    })
this.getdistrict();
this.listdepttype();

  }

listdepttype(){
console.log("OOOOOOOOOO");
    const api = `http://34.93.47.253:9004/mla_server/api/v1/departmenttype`
  axios.get(api, {
      headers: {
        "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE", 'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
                console.log(res.data);
        });
       // this.state.data.rows = res.data.data

        // console.log(this.state.data)



}
cancelclick(){
this.setState({
department_type:'',
districtdata:'',
talukdata:'',
warddata:'',
pincode:''
       })
this.componentDidMount();
}
 filterevent(event) {
console.log("event",event.target);
const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });

console.log("state",this.state);
  }

     filtersubmit(event) {
event.preventDefault();
    //alert('A name was submitted: ' + this.state.value);
console.log(" this.state.value");
var apidata={
department: this.state.department_type,
taluk: this.state.talukdata,
district:this.state.districtdata,
panchayath_ward:this.state.warddata,
pin:this.state.pincode,

}
  const api = `http://34.93.47.253:9004/mla_server/api/v1/list_departmentcontact_by_department`
axios.post(api,apidata,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
            this.setState({ 'droplets': res.data.data });
}else{
        this.setState({'droplets':'' })

}
       
    })
  }
activatedata(id){

toastr.confirm('Are you sure confirm to activate the department?',{
 okText:     <button className="alertok" onClick={() => this.activate(id)} >Ok</button>,
  buttons: [{
    okText: 'Do not apply',
    className: 'do-not-apply-btn',
    handler: () => console.log('do-not-apply clicked')
  }, {
    cancel: true // move the cancel button to the end
  }]

});


}
deactivatedata(id){

toastr.confirm('Are you sure confirm to deactivate the department?',{
okText:     <button className="alertok" onClick={() => this.deactivate(id)} >Ok</button>,
  buttons: [{
    okText: 'Do not apply',
    className: 'do-not-apply-btn',
    handler: () => console.log('do-not-apply clicked')
  }, {
    cancel: true // move the cancel button to the end
  }]
  

});


}

activate(id){
//toastr.success('The title', 'The message')
const api = `http://34.93.47.253:9004/mla_server/api/v1/deactivate_department_contact`
axios.post(api,{
 department_contact_id: id,
    is_active:true
  }, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
this.props.history.push('/department/departmentlist');
    window.location.reload(); 


       
    })
}
deactivate(id){
console.log("ssdd",id);
const api = `http://34.93.47.253:9004/mla_server/api/v1/deactivate_department_contact`
axios.post(api,{
 department_contact_id: id,
    is_active:false
  }, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
this.props.history.push('/department/departmentlist');
    window.location.reload(); 
       
    })
}
getdistrict(){
this.district_list=[];
  const api = `http://34.93.47.253:9004/mla_server/api/v1/filter_category`
axios.get(api,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
        this.state.district_list = res.data.complaint_categories.district;
            console.log("this.district_list",this.state.district_list);

}else{


} 
 })

}
getdistrictdata(event){
            this.setState({ 'districtdata': event.target.value});
	this.loadTaluk(event);

}
  loadTaluk(eventdata: any) {
    console.log("eventdata", eventdata.target.value);
console.log("this.district_list",this.state.district_list);
    var datalist = this.state.district_list.find(o => o.name == eventdata.target.value) || '';
    this.state.taluk_list = datalist.taluk;
    console.log("datalistssd", datalist)


    
  }

  loadPanchayat(eventdata: any){
            this.setState({ 'talukdata': eventdata.target.value});
    var datalist = this.state.taluk_list.find(o => o.name == eventdata.target.value) || '';
    this.state.ward_list = datalist.panchayath_ward;
    console.log("this.state.ward_list", this.state.ward_list)
  }

wardevent(eventdata: any){
    console.log("eventdata", eventdata.target.value)
    var datalist = this.state.ward_list.find(o => o.name == eventdata.target.value) || '';
            this.setState({ 'warddata': eventdata.target.value});
            this.setState({ 'pincode': datalist.pin});

}


viewdata(id){
console.log("id",id);
    localStorage.setItem('deptid', id);
    const deptidvalue = localStorage.getItem('deptid');
console.log("deptidvalue",deptidvalue);
}

edit(id){
console.log("id",id);
    localStorage.setItem('deptid', id);
    const deptidvalue = localStorage.getItem('deptid');
console.log("deptidvalue",deptidvalue);
}
     render() {
const index=0;
        return (
           <div  >
<form onSubmit={this.filtersubmit}>
<div className="row">
<div className="form-group col-md-12" >
              <h1 id='title' className="floatleft">List of Departments</h1>
</div>
</div>
     <div className="form-group col-md-12">
          <h4> Filter Departments  </h4>
</div>
<div className="row">
                  <div className="form-group col-md-4">

<label>Department Type</label>
<select className="slectbox"  className="width" value={this.state.department_type}   name="department_type"  placeholder="Choose showFirstLastButtons" onChange={this.filterevent}
                    >
                    <option value="" disabled selected>Choose Department Type </option>
                    <option value='Road'>Road</option>
                    <option value='Garbage'>Garbage </option>
                    <option value='Water'>Water </option>
                    <option value='Transport'>Transport </option>

                  </select>
</div>

   <div className="form-group col-md-4">

<label>Select District</label>
<select className="slectbox"  className="width" placeholder="Choose showFirstLastButtons"  onChange={this.getdistrictdata} value={this.state.districtdata}   name="districtdata">
                    <option value="" disabled selected>Choose District </option>
                 {this.state.district_list.map(optn => (

                     <option>{optn.name}</option>
                 ))}
             </select>
</div>


<div className="form-group col-md-4">

<label>Select Taluk</label>
<select className="slectbox"  className="width" placeholder="Choose showFirstLastButtons"  onChange={this.loadPanchayat}
  value={this.state.talukdata}   name="talukdata"                   >
                   <option value="" disabled selected>Choose Taluk </option>
                   {this.state.taluk_list.map(optn => (

                     <option>{optn.name}</option>
                 ))}


                  </select>
</div>
</div>


<div className="row">
<div className="form-group col-md-4">
<label>Select Ward</label>
<select className="slectbox"  className="width"  placeholder="Choose showFirstLastButtons" onChange={this.wardevent}
  value={this.state.warddata}   name="warddata"        
                    >
                   <option value="" disabled selected>Choose Ward </option>
                   {this.state.ward_list.map(optn => (

                     <option>{optn.name}</option>
                 ))}
                  </select>
</div>


<div className="form-group col-md-4">
<label>Pincode</label>
<input type="text" value={this.state.pincode} name="pincode"  className="width" onChange={this.filterevent} />
</div>
</div>



                <button  type="submit" value="Submit" className="btn btn-primary">Submit</button>
              <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>

</form>
<Link to="/department/departmentadd">
    <button className="btn btn-primary floatright">Add Department</button>
</Link>
<div>
     <table className="table table-hover">
      <thead>
        <tr className="bgcolor">
          <th>Sl.No</th>
          <th>Department Type</th>
          <th>EmailId</th>
          <th>Phone number</th>
          <th>Contact Person</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        { (this.state.droplets.length > 0) ? this.state.droplets.map( (droplet, index) => {
           return (
            <tr key={ index }>
              <td>{ index+1 }</td>
              <td>{ droplet.department }</td>
              <td>{ droplet.email}</td>
              <td>{ droplet.phone_number }</td>
              <td>{ droplet.contact_person }</td>
              <td>
<Link to="/department/departmentview">
    <button className="btn btn-view" onClick={() => this.viewdata(droplet.department_contact_id)}>View</button>
</Link>

{droplet.is_active ? (
<div >
    <button className="btn btn-green" onClick={() => this.deactivatedata(droplet.department_contact_id)} >Deactivate</button>
</div>
) : <div >
    <button className="btn btn-green" onClick={() => this.activatedata(droplet.department_contact_id)} >Activate</button>
</div>}

<Link to="/department/departmentedit">
    <button className="btn btn-edit" onClick={() => this.edit(droplet.department_contact_id)}>Edit</button>
</Link>
</td>
            </tr>
          )
         }) : <tr><td colSpan="5">No records found</td></tr> }
      </tbody>
    </table>
</div>
           </div>
        )
     }
}
