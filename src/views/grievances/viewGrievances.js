import React, { Component } from "react";
import {withRouter, Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
import { Map, GoogleApiWrapper, Marker} from 'google-maps-react';
const mapStyles = {
  width: '50%',
  height: '50%'
};
const validateForm = errors => {
  let valid = true;
  Object.values(errors).forEach(val => val.length > 0 && (valid = false));
  return valid;
};
export class ViewDepartment extends Component {



 constructor(props){
      super(props);
    this.state = {
      grievance: [],
	lat:[],
	lng:[],
	status:[],
	id:[],
status_description:[],
show:false,
errors:{
status_description:''
}
    }
this.statusvalue = [
    { value: 0, name: 'Open' },
    { value: 3, name: 'Inprogress' },
    { value: 1, name: 'Irrelavant' },
    { value: 2, name: 'Completed' },
                                                        
];
this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangetextarea = this.handleChangetextarea.bind(this);

   }

handleSubmit(event) {
event.preventDefault();
console.log("sdsds",this.state);
if(this.state.status=='Open'){
this.state.status='0';
}
if(this.state.status=='Inprogress'){
this.state.status='3';
}
if(this.state.status=='Irrelavant'){
this.state.status='1';
}
if(this.state.status=='Completed'){
this.state.status='2';
}
console.log("this.state.status",this.state.status)
var data={
complaint_id: this.state.id,
complaint_status:  this.state.status,
status_description: this.state.status_description
}
  const api = `http://35.193.187.77:9004/mla_server/api/v1/update_complaint_status`
axios.post(api,data,
  { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} }
  )
        .then(res => {
if(res.data.statuscode==200){
this.props.history.push('/grievances/listgrievances');
toastr.success('Status updated successfuly')

            console.log(res.data);
}
            console.log(res.data);
    })
}

handleChangetextarea(event){
const target = event.target;
    const value = target.value;
    const name = target.name;
 this.setState({
     'status_description': value
    });

}
componentDidMount(){
    const deptidvalue = localStorage.getItem('complaintid');
console.log("deptidvalue",deptidvalue);
   const api = `http://35.193.187.77:9004/mla_server/api/v1/get_complaint?complaint_id=`
axios.get(api+deptidvalue, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
console.log("ressss",res.data.data.complaint_status);
var statusvalue;
if(res.data.data.complaint_status==0){
console.log("inside open");
this.setState({'status':'Open'})
}
if(res.data.data.complaint_status==1){

this.setState({'status':'Irrelevant'})
}
if(res.data.data.complaint_status==2){
console.log("inside Completed");
this.setState({'status':'Completed'})
}
if(res.data.data.complaint_status==3){
this.setState({'status':'Inprogress'})
};
            this.setState({ 'grievance': res.data.data ,
'lat':res.data.data.location[0],
'lng':res.data.data.location[1],
'id':res.data.data.complaint_id,
'status_description':res.data.data.status_description
});
       console.log("sdjhfhsdf",this.state);
       
    })
  }

handleChange(event) {
console.log("event",event.target.value);

    let errors = this.state.errors;
this.show=true;

const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
     'status': value
    });
    this.setState({errors,'status': value});
}
    render() {
    const {errors} = this.state;
        return (
           <form onSubmit={this.handleSubmit} >

                <div className="row" >
                    <div className="form-group col-md-6">
                        <h3 className="floatleft" id='title'>Grievance Details</h3>
                    </div>
                    <div className="form-group col-md-6">
                <button  type="submit" value="Submit" className="btn btn-primary margin">Submit</button>
                        <Link className="nav-link floatright" to={"/grievances/listgrievances"}>Back</Link>
                    </div>

                </div>


                <div className="form-group">
                    <label className="floatleft padright" >Grievance Description:</label>
                    {this.state.grievance.complaint_description}

                </div>

                <div className="form-group">
                    <label className="floatleft padright">Grievance Category:</label>{this.state.grievance.complaint_category}

                </div>
                <div className="form-group">
                    <label className="floatleft padright">Grievance Registered On</label> {this.state.grievance.created_at}

                </div>

                <div className="form-group ">
                    <label className="floatleft padright">Grievance Status:</label> 

<select name="status" value={this.state.status}  onChange={this.handleChange} >
                   <option value={this.state.status} >{this.state.status} </option>
 {this.statusvalue.map(optn => (

                     <option key={optn} value={optn.name}>{optn.name}</option>
                 ))}
</select>
                </div>
                <div className="form-group">
                    <label className="floatleft padright">Status Description</label> 
<textarea value={this.state.status_description} name="status_description" onChange={this.handleChangetextarea} >

</textarea>
{this.show? (
                        <div >
                          <p className='error'>Enter reason to change the status</p>
                        </div>
                      ) : null}
                </div>

                <div className="form-group">
                    <label className="floatleft padright">Complaint location details : </label>
                </div>

<div className="form-group">
                    <label className="floatleft padright">Village :</label>{this.state.grievance.village_panchayat}

                </div>
<div className="form-group">
                    <label className="floatleft padright">Taluk :</label>{this.state.grievance.taluk}

                </div>
                <div className="form-group">
                    <label className="floatleft padright">Grievance Given by :</label> 
                </div>

  <div className="form-group">
                    <label className="floatleft padright">Name:</label> {this.state.grievance.first_name}

                </div>
 <div className="form-group">
                    <label className="floatleft padright">Date of Birth:</label> {this.state.grievance.dob}

                </div>
<div className="form-group">
                    <label className="floatleft padright">Contact No:</label> {this.state.grievance.user_contact_no}

                </div>
<div className="form-group">
                    <label className="floatleft padright">Age :</label> {this.state.grievance.age}

                </div>
<div className="form-group">
                    <label className="floatleft padright">Mail-id :</label> {this.state.grievance.user_mail_id}

                </div>
<div className="form-group">
                    <label className="floatleft padright">Address :</label> {this.state.grievance.address1}

                </div>
<div className="form-group">
                    <label className="floatleft padright">Gender :</label> {this.state.grievance.gender}

                </div>
<div className="form-group">
 <img className="complaint" src={this.state.grievance.complaint_images} />

     
           </div>

      <div className="form-group">
<Map
      google={this.props.google}
       zoom={14}
        style={mapStyles}
                 initialCenter={{ lat:13.59, lng: 75.206}}>
          <Marker position={{ lat:this.state.lat, lng: this.state.lng}} />

        </Map>
</div>
    <div>


</div>
            </form>
        )

    }
}
export default GoogleApiWrapper({
  apiKey: 'AIzaSyB-i-geNyumo0S8t3i9NWnMK0bae5qV4xo'
})(ViewDepartment);

