import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
import { MDBDataTableV5 } from 'mdbreact';

export default class ListGrievances extends Component {

  
  constructor(props) {

   
  
    super(props);
    this.state = {
      droplets: '',
      showHide: true,
      complaint_status: '',
      complaint_category: '',
district:'',
      taluk: '',
      town_panchayat: '',
      village_panchayat: '',
panchayath_ward:'',
      ward_no: '',
      open: false,
taluk_list:[],
ward_list:[],
district_list:[],
district:'',
      
    }

this.statusvalue = [
    { value: 0, name: 'Open' },
    { value: 3, name: 'Inprogress' },
    { value: 1, name: 'Irrelavant' },
    { value: 2, name: 'Completed' },
                                                        
];
    this.storeview = this.storeview.bind(this);
    this.filterevent = this.filterevent.bind(this);
    this.filtersubmit = this.filtersubmit.bind(this);
this.getdistrictdata = this.getdistrictdata.bind(this);
    this.loadPanchayat = this.loadPanchayat.bind(this);
    this.wardevent = this.wardevent.bind(this);


  }


  componentDidMount() {
    console.log("sdjhfhsdf");
    const api = `http://35.193.187.77:9004/mla_server/api/v1/list_complaints`
    axios.get(api, {
      headers: {
        "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE", 'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        this.setState({
          'droplets': res.data.data,
          showHide: true

        });
       // this.state.data.rows = res.data.data
        console.log(this.state.droplets);
        // console.log(this.state.data)

      })
this.getdistrict();


  }
  cancelclick() {
    this.setState({
      complaint_status: '',
      complaint_category: '',
      taluk: '',
      town_panchayat: '',
      village_panchayat: '',
      ward_no: '',
district:'',
panchayath_ward:'',
    })
    this.componentDidMount();
  }

  togglePanel(e) {
    console.log("sdsdsd")
    this.setState({ open: !this.state.open })
  }
  filterevent(event) {
    console.log("event", event.target);
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });

    console.log("state", this.state);
  }

  filtersubmit(event) {
    event.preventDefault();
    //alert('A name was submitted: ' + this.state.value);
    console.log(" this.state.value", this.state);
    var apidata = {
      complaint_status: this.state.complaint_status,
      complaint_category: this.state.complaint_category,
      taluk: this.state.taluk,
      district: this.state.district,
      panchayath_ward: this.state.panchayath_ward,
      town_panchayat: this.state.town_panchayat,
      village_panchayat: this.state.village_panchayat,
      ward_no: this.state.ward_no

    }
    const api = `http://35.193.187.77:9004/mla_server/api/v1/complaints_filter`
    axios.post(api, apidata, {
      headers: {
        Authorization: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE", 'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        console.log(res.data);
        if (res.data.statuscode == 200) {
          this.setState({ 'droplets': res.data.data });
         
        } else {
          this.setState({ 'droplets': '' })

        }

      })
  }

  storeview(id) {
    console.log("id", id);
    localStorage.setItem('complaintid', id);
  }

getdistrict(){
this.district_list=[];
  const api = `http://35.193.187.77:9004/mla_server/api/v1/filter_category`
axios.get(api,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
        this.state.district_list = res.data.complaint_categories.district;
            console.log("this.district_list",this.state.district_list);

}else{


} 
 })

}
getdistrictdata(event){
            this.setState({ 'district': event.target.value});
	this.loadTaluk(event);

}
  loadTaluk(eventdata: any) {
    console.log("eventdata", eventdata.target.value);
console.log("this.district_list",this.state.district_list);
    var datalist = this.state.district_list.find(o => o.name == eventdata.target.value) || '';
    this.state.taluk_list = datalist.taluk;
    console.log("datalistssd", datalist)


    
  }

  loadPanchayat(eventdata: any){
            this.setState({ 'taluk': eventdata.target.value});
    var datalist = this.state.taluk_list.find(o => o.name == eventdata.target.value) || '';
    this.state.ward_list = datalist.panchayath_ward;
    console.log("this.state.ward_list", this.state.ward_list)
  }

wardevent(eventdata: any){
    console.log("eventdata", eventdata.target.value)
    var datalist = this.state.ward_list.find(o => o.name == eventdata.target.value) || '';
            this.setState({ 'panchayath_ward': eventdata.target.value});
            this.setState({ 'pincode': datalist.pin});

}

  render() {
    return (

      <div>
      
          <div className="row">
            <div className="form-group col-md-12" >
              <p>
                <h4 id='title'  className="floatleft heading-color">List of Grievances</h4>
              </p>
            </div>
            <div  >

            </div>
          </div>
          
          <form onSubmit={this.filtersubmit} class="pb-10">
          <div className="row">
            <div className="form-group col-md-12" >
                          <h5> Filter Grievances  </h5>
          </div>
          </div>
          <div className="row">
            <div className="form-group col-md-4">

              <label>Select Status</label>
              <select className="slectbox" className=" width" value={this.state.complaint_status} name="complaint_status" onChange={this.filterevent} placeholder="Choose showFirstLastButtons"
              >
                <option value="" disabled selected>Choose complaint status </option>
                <option value='0'>Open </option>
                <option value='3'>Inprogress </option>
                <option value='1'>Irrelevant </option>
                <option value='2'>Completed</option>



              </select>
            </div>

            <div className="form-group col-md-4">

              <label>Select Category</label>
              <select className="slectbox" className="width" placeholder="Choose showFirstLastButtons" value={this.state.complaint_category} name="complaint_category" onChange={this.filterevent}
              >
                <option value="" disabled selected>Select Category </option>

                <option value="Drainage">Drainage </option>
                <option value="Education">Education </option>
                <option value="Garbage">Garbage </option>
                <option value="Road">Road </option>
                <option value="Transport">Transport </option>
                <option value="Water">Water</option>

              </select>
            </div>
 <div className="form-group col-md-4">
<label>Select District</label>
            <select className="slectbox" className="width" placeholder="Choose showFirstLastButtons" onChange={this.getdistrictdata} value={this.state.district}   name="district"
              >
                <option value="" disabled selected>Choose District </option>
                 {this.state.district_list.map(optn => (

                     <option>{optn.name}</option>
                 ))}
             </select>
            </div>

          </div>
          <div className="row">

            <div className="form-group col-md-4" >
 <label>Select Taluk</label>
  <select className="slectbox" className="width" placeholder="Choose showFirstLastButtons" onChange={this.loadPanchayat} value={this.state.taluk}   name="taluk"
              >
               
                   <option value="" disabled selected>Choose Taluk </option>
                   {this.state.taluk_list.map(optn => (

                     <option>{optn.name}</option>
                 ))}


                  </select>
</div>
          
            <div className="form-group col-md-4" >
              <label>Select Ward</label>
<select className="slectbox" className="width" placeholder="Choose showFirstLastButtons" onChange={this.wardevent}  value={this.state.panchayath_ward}   name="panchayath_ward"
              >
                   <option value="" disabled selected>Choose Ward </option>
                   {this.state.ward_list.map(optn => (

                     <option>{optn.name}</option>
                 ))}
                  </select>
            </div>
         
           
          </div>


          <button type="submit" value="Submit" className="btn btn-primary">Submit</button>
          <button onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>

        </form>

        <div>

        {/* <MDBDataTableV5 hover entriesOptions={[5, 20, 25]} entries={5} pagesAmount={4} data={this.state.droplets} />; */}
           <table className="table table-hover">
            <thead>
              <tr className="bgcolor">
                <th>Slno</th>
                <th>Day of grievance</th>
                <th>Category</th>
                <th>Description</th>
                <th>Taluk</th>
                <th>Grievance Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {(this.state.droplets.length > 0) ? this.state.droplets.map((droplet, index) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{droplet.created_at}</td>
                    <td>{droplet.complaint_category}</td>
                    <td>{(droplet.complaint_description.length > 20) ? (droplet.complaint_description.slice(0, 10)) + '..' : (droplet.complaint_description)}</td>
                    <td>{droplet.taluk}</td>

                    <td >

                      {droplet.complaint_status=='0'? (
                        <div >
                          <p>Open</p>
                        </div>
                      ) : null}
{droplet.complaint_status=='2'? (
                        <div >
                          <p>Completed</p>
                        </div>
                      ) : null}
{droplet.complaint_status=='3'? (
                        <div >
                          <p>Inprogress</p>
                        </div>
                      ) : null}
{droplet.complaint_status=='1'? (
                        <div >
                          <p>Irrelavant</p>
                        </div>
                      ) : null}
                    </td>
                    <td><Link to="/grievances/viewGrievances">
                      <button className="btn btn-view" 

 onClick={() => this.storeview(droplet.complaint_id)}>View</button>
</Link>
                    </td>

                  </tr>
                )
              }) :
                <tr><td colSpan="5" >No records found</td></tr>


              }
            </tbody>
          </table> 
           
        </div>
      </div>

    )
  }
}

