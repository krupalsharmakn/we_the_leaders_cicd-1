import React, { Component } from "react";
import {withRouter, Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
export  default class EditPartyworker extends Component {
constructor(props){
      super(props);
    this.state = {
party_worker_id:'',
name: '',
email:'',
phone_number:'',
alternative_number:'',
DOB:'',
gender:'',
party_worker_type:'',
panchayath_ward:'',
taluk:'',
district:'',
age:'',
age_range:'',
pin:'',
blood_group:'',
adhaar_number:'',
father_husband:'',
partyworker_images:'',
position_of_partyworker:'',
address1:'',
address2:'',
address3:'',
education:'',
district_list:[],
taluk_list:[],
ward_list:[],
};
      this.onChange = this.onChange.bind(this);
this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

   }


componentDidMount(){
    console.log("sdjhfhsdf");
    const workervalue = localStorage.getItem('partyworkerid');
   const api = `http://34.93.47.253:9004/mla_server/api/v1/get_partyworker?party_worker_id=`
axios.get(api+workervalue, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
            this.setState({ 
party_worker_id:res.data.data.party_worker_id,
name: res.data.data.name,
email:res.data.data.email,
phone_number:res.data.data.phone_number,
alternative_number:res.data.data.alternative_number,
DOB:res.data.data.DOB,
gender:res.data.data.gender,
party_worker_type:res.data.data.party_worker_type,
panchayath_ward:res.data.data.panchayath_ward,
taluk:res.data.data.taluk,
district:res.data.data.district,
age:res.data.data.age,
age_range:res.data.data.age_range,
pin:res.data.data.pin,
blood_group:res.data.data.blood_group,
adhaar_number:res.data.data.adhaar_number,
father_husband:res.data.data.father_husband,
position_of_partyworker:res.data.data.position_of_partyworker,
partyworker_images:res.data.data.partyworker_images,
addresss1:res.data.data.addresss1,
addresss2:res.data.data.addresss2,
addresss3:res.data.data.addresss3,
education:res.data.data.education,

});

       
    })
this.getdistrict();
  }

     handleFormReset = () => {
 this.state = {
name: '',
email:'',
phone_number:'',
alternative_number:'',
DOB:'',
gender:'',
party_worker_type:'',
panchayath_ward:'',
taluk:'',
district:'',
age:'',
age_range:'',
pin:''};
    this.setState(() => this.state)
  }
   
    onChange(e){
console.log("sdsddonchange");
     const re = /^[0-9\b]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
  this.handleChange(e)
        this.setState({value: e.target.value})
      }
 }

twoCalls = e => {

  this.onChange(e)
}
cancelclick(){
 this.props.history.push('/theme/colors');
}
 handleChange(event) {
console.log("event",event.target);
const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    //alert('A name was submitted: ' + this.state.value);
event.preventDefault();
console.log(" this.state.value", this.state);
  const api = `http://34.93.47.253:9004/mla_server/api/v1/edit_partyworker`
axios.post(api,this.state,{ headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {

 this.props.history.push('/theme/colors');
toastr.success('Party worker updated successfuly')
            console.log(res.data);
    })
  }

getdistrict(){
  const api = `http://34.93.47.253:9004/mla_server/api/v1/filter_category`
axios.get(api,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
        this.state.district_list = res.data.complaint_categories.district;
            console.log("this.district_list",this.state.district_list);

}else{


} 
 })

}
getdistrictdata(event){
            this.setState({ 'district': event.target.value});
	this.loadTaluk(event);

}
  loadTaluk(eventdata: any) {
    console.log("eventdata", eventdata.target.value);
console.log("this.district_list",this.state.district_list);
    var datalist = this.state.district_list.find(o => o.name == eventdata.target.value) || '';
    this.state.taluk_list = datalist.taluk;
    console.log("datalistssd", datalist)


    
  }

  loadPanchayat(eventdata: any){
            this.setState({ 'taluk': eventdata.target.value});
    var datalist = this.state.taluk_list.find(o => o.name == eventdata.target.value) || '';
    this.state.ward_list = datalist.panchayath_ward;
    console.log("this.state.ward_list", this.state.ward_list)
  }

wardevent(eventdata: any){
    console.log("eventdata", eventdata.target.value)
    var datalist = this.state.ward_list.find(o => o.name == eventdata.target.value) || '';
            this.setState({ 'panchayath_ward': eventdata.target.value});
            this.setState({ 'pin': datalist.pin});

}
render() {
return (
           <form onSubmit={this.handleSubmit}>
<div   className="row" >
                <div className="form-group col-md-6">
                <h3 className="floatleft" id='title'>Edit Party Worker</h3>
</div>
                <div className="form-group col-md-6">
                <Link className="nav-link floatright" to={"/theme/colors"}>Back</Link>
</div>
</div>
<div   className="row" >
                <div className="form-group col-md-6">
                    <label>Name</label>
                    <input type="text" className="form-control" value={this.state.name} name="name" onChange={this.handleChange} maxlength="20"/>
                </div>
                <div className="form-group col-md-6">
 <label>Phone number</label>
<input className="form-control"  value={this.state.phone_number}   name="phone_number"  onChange={this.twoCalls} maxlength="10"/>

</div>

</div>
<div   className="row" >
                <div className="form-group  col-md-6">
                    <label> Alternative number</label>
                    <input type="text" className="form-control" value={this.state.alternative_number}   onChange={this.twoCalls} name="alternative_number"  maxlength="10" />
                </div>
  <div className="form-group  col-md-6">
                    <label>Email Id</label>
                    <input type="email" value={this.state.email} name="email" className="form-control" onChange={this.handleChange} />
                </div>
                
</div>


<div   className="row" >
                <div className="form-group  col-md-6">
                    <label> DOB</label>
<input type="date"  className="form-control" value={this.state.DOB} name="DOB"  onChange={this.handleChange} />

                </div>
  <div className="form-group  col-md-6">
                    <label>Select Worker Type:</label>
<select className="slectbox"  className="form-control width" value={this.state.party_worker_type} name="party_worker_type" onChange={this.handleChange}  placeholder="Choose showFirstLastButtons" onChange={this.handleChange}
                    >
                    <option value="" disabled selected>Choose Worker Type </option>
                    <option value="Youth Member"> Youth Member</option>
                    <option value="Women Member">Woman Member</option>
                    <option value="Panchayat">Panchayat / Ward Member</option>


                  </select>
                </div>
                
</div>
<div   className="row" >
                <div className="form-group  col-md-6">
                    <label> Aadhar Number</label>
<input type="text"  className="form-control" value={this.state.adhaar_number} name="adhaar_number"  onChange={this.handleChange} maxlength="12"/>

                </div>
       <div className="form-group  col-md-6">
                    <label> Blood Group</label>
<input type="text"  className="form-control" value={this.state.blood_group} name="blood_group"  onChange={this.handleChange} />

                </div>
                
</div>

<div  className="row">
<div className="form-group  col-md-6">
                    <label> Gender</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons" value={this.state.gender} name="gender" placeholder="Choose showFirstLastButtons" onChange={this.handleChange}
                    >
                    <option value="" disabled selected>Choose option </option>
                    <option value="Male"> Male</option>
                    <option value="Female">Female </option>
                    <option value="Other">Other </option>


                  </select>

                </div>
 <div className="form-group  col-md-6">
                    <label> Father/Husband Name</label>
<input type="text"  className="form-control" value={this.state.father_husband} name="father_husband"  onChange={this.handleChange} maxlength="20" />

                </div>

 
</div>

<div  className="row">
 <div className="form-group  col-md-6">
                    <label> Education qualification</label>
<input type="text"  className="form-control" value={this.state.education} name="education" maxlength="15" onChange={this.handleChange} />

                </div>
<div className="form-group  col-md-6">
                    <label> Position of Party worker</label>
<select className="slectbox"  className="form-control width" value={this.state.position_of_partyworker} name="position_of_partyworker" onChange={this.handleChange}  placeholder="Choose showFirstLastButtons" onChange={this.handleChange} noValidate
                    >
                    <option value="" disabled selected>Choose Worker Type </option>
                    <option value="Youth Member"> Youth Member</option>
                    <option value="Women Member">Woman Member</option>
                    <option value="Panchayat">Panchayat / Ward Member</option>


                  </select>

                </div>

</div>



<div  className="row">
<div className="form-group  col-md-6">
                    <label> Address 1</label>
<textarea type="text"  placeholder="Building No., Flat no"  className="form-control" value={this.state.address1} name="address1"  onChange={this.handleChange} maxlength="150" />

                </div>

<div className="form-group  col-md-6">
                    <label> Address 2</label>
<textarea type="text"  placeholder="Street "  className="form-control" value={this.state.address2} name="address2"  onChange={this.handleChange} maxlength="150" />

                </div>
</div>
<div  className="row">
<div className="form-group  col-md-6">
                    <label> Address 3</label>
<textarea type="text"  placeholder="Street "  className="form-control" value={this.state.address3} name="address3"  onChange={this.handleChange} maxlength="200" />

                </div>
       <div className="form-group  col-md-6">
<label>Select District</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons"  onChange={this.getdistrictdata} value={this.state.district}   name="district">
                    <option value="" disabled selected>Choose District </option>
                 {this.state.district_list.map( key => (

                     <option  key={key} value={key.name} >{key.name}</option>
                 ))}
             </select>

</div>
</div>

<div  className="row">
<div className="form-group  col-md-6">
<label>Select Taluk :</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons"  onChange={this.loadPanchayat}
  value={this.state.taluk}   name="taluk"                   >
                   <option value="" disabled selected>Choose Taluk </option>
                   <option value={this.state.taluk} >{this.state.taluk} </option>
                   {this.state.taluk_list.map(optn => (

                     <option key={optn} value={optn.name}>{optn.name}</option>
                 ))}


                  </select>
</div>
 <div className="form-group  col-md-6">
<label>Select Ward :</label>
<select className="slectbox"  className="form-control width"  placeholder="Choose showFirstLastButtons" onChange={this.wardevent}
  value={this.state.panchayath_ward}   name="panchayath_ward"        
                    >
                   <option value="" disabled selected>Choose Ward </option>
                   <option value={this.state.panchayath_ward} >{this.state.panchayath_ward} </option>
                   {this.state.ward_list.map(optn => (

                     <option>{optn.name}</option>
                 ))}
                  </select>
</div>
         
</div>
<div  className="row">
 <div className="form-group  col-md-6">
<label>PIN code</label>
  <input type="text" className="form-control" value={this.state.pin} name="pin"  onChange={this.handleChange}  maxlength="06"/>
</div>
</div>
<div className="form-group">               
 <img src={this.state.partyworker_images} />
                </div>
                <button type="submit"  value="Submit" className="btn btn-primary">Submit</button>
                            <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>
                
            </form>
)

}
}
