import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
export  default class AddPartyworker extends Component {
constructor(){
      super();
    this.state = {
name: '',
email:'',
phone_number:'',
alternative_number:'',
DOB:'',
gender:'',
party_worker_type:'',
panchayath_ward:'',
taluk:'',
district:'',
age:'',
age_range:'',
pin:''};
    //  this.onChange = this.onChange.bind(this);
this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

   }
   
   onChange(e){
      const re = /^[0-9\b]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
         this.setState({value: e.target.value})
      }
   }

 handleChange(event) {
console.log("event",event.target);
const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    //alert('A name was submitted: ' + this.state.value);
console.log(" this.state.value", this.state);
  const api = `http://34.93.47.253:9004/mla_server/api/v1/add_partyworker`
//axios.post(api,{ headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  //'Content-Type': 'application/json'},
//body:this.state  })
        //.then(res => {

          //  console.log(res.data);



       
   // })
  }
render() {
return (
           <form onSubmit={this.handleSubmit}>
<div   className="row" >
                <div className="form-group col-md-6">
                <h3 className="floatleft" id='title'>Add Party Worker</h3>
</div>
                <div className="form-group col-md-6">
                <Link className="nav-link floatright" to={"/theme/colors"}>Back</Link>
</div>
</div>
<div   className="row" >
                <div className="form-group col-md-6">
                    <label>Name</label>
                    <input type="text" className="form-control" value={this.state.name} name="name" onChange={this.handleChange} />
                </div>
                <div className="form-group col-md-6">
 <label>Phone number</label>
<input className="form-control"  value={this.state.phone_number}   name="phone_number" onChange={this.handleChange} maxlength="10"/>

</div>

</div>
<div   className="row" >
                <div className="form-group  col-md-6">
                    <label> Alternative number</label>
                    <input type="text" className="form-control" value={this.state.alternative_number}  onChange={this.handleChange} name="alternative_number"  />
                </div>
  <div className="form-group  col-md-6">
                    <label>Email Id</label>
                    <input type="email" value={this.state.email} name="email" className="form-control" onChange={this.handleChange} />
                </div>
                
</div>


<div   className="row" >
                <div className="form-group  col-md-6">
                    <label> DOB</label>
<input type="date"  className="form-control" value={this.state.DOB} name="DOB"  onChange={this.handleChange} />

                </div>
  <div className="form-group  col-md-6">
                    <label>Select Worker Type:</label>
<select className="slectbox"  className="form-control width" value={this.state.party_worker_type} name="party_worker_type" onChange={this.handleChange}  placeholder="Choose showFirstLastButtons" onChange={this.handleChange}
                    >
                    <option value="" disabled selected>Choose Worker Type </option>
                    <option value="Youth Member"> Youth Member</option>
                    <option value="Women Member">Woman Member</option>
                    <option value="Panchayat">Panchayat / Ward Member</option>


                  </select>
                </div>
                
</div>


<div  className="row">
                <div className="form-group  col-md-6">
<label>Select District :</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons" value={this.state.district} name="district" placeholder="Choose showFirstLastButtons" onChange={this.handleChange}
                    >
                    <option value="" disabled selected>Choose District </option>
                    <option value="Shimoga"> Shimoga</option>
                    <option value="Bengalore">Bengalore </option>
                    <option value="Belagavi">Belagavi </option>


                  </select>
</div>

 <div className="form-group  col-md-6">
<label>Select Taluk :</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons" value={this.state.taluk} name="taluk"  onChange={this.handleChange}
                    >
                    <option value="" disabled selected>Choose Taluk </option>
                    <option value="Shimoga"> Shimoga</option>
                    <option value="Bengalore">Bengalore </option>
                    <option value="Belagavi">Belagavi </option>


                  </select>
</div>

</div>

<div  className="row">
                <div className="form-group  col-md-6">
<label>Select Ward :</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons" value={this.state.panchayath_ward} name="panchayath_ward"  onChange={this.handleChange}
                    >
                    <option value="" disabled selected>Choose Ward </option>
                    <option value="Shimoga"> Shimoga</option>
                    <option value="Bengalore">Bengalore </option>
                    <option value="Belagavi">Belagavi </option>


                  </select>
</div>

 <div className="form-group  col-md-6">
<label>PIN code</label>
  <input type="text" className="form-control" value={this.state.pin} name="pin"  onChange={this.handleChange} />
</div>

</div>
                <button type="submit"  value="Submit" className="btn btn-primary">Submit</button>
                
            </form>
)

}
}
