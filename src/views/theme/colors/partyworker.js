import React, { Component } from "react";
import {withRouter, Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
import { MDBTable, MDBTableBody, MDBTableHead, MDBDataTable } from 'mdbreact';

export  default class Partyworker extends Component {
    constructor(props){
       super(props);
       this.state = {
      droplets: [],
taluk:'',
town_panchayat:'',
village_panchayat:'',
ward_no:'',
worker_type:'',
age_range:'',
gender:'',
district:'',
taluk_list:[],
ward_list:[],
district_list:[],
panchayath_ward:'',
pincode:''

    }
          this.viewdata = this.viewdata.bind(this);
      this.filterevent = this.filterevent.bind(this);
    this.filtersubmit = this.filtersubmit.bind(this);
this.getdistrictdata = this.getdistrictdata.bind(this);
    this.loadPanchayat = this.loadPanchayat.bind(this);
    this.wardevent = this.wardevent.bind(this);
   }
 
componentDidMount(){
       console.log("sdjhfhsdf");
   const api = `http://34.93.47.253:9004/mla_server/api/v1/list_partyworkers`
axios.get(api, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
this.listdept=res.data.data;
            console.log(res.data.data);
console.log("this.listdeptlist",this.listdept.length);
            this.setState({ 'droplets': res.data.data });

       
    })
this.getdistrict();

  }
cancelclick(){
this.setState({
taluk:'',
town_panchayat:'',
village_panchayat:'',
ward_no:'',
worker_type:'',
age_range:'',
gender:'',
district:'',
panchayath_ward:'',
pincode:''
       })
this.componentDidMount();
}

filterevent(event) {
console.log("event",event.target);
const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });

console.log("state",this.state);
  }


     filtersubmit(event) {
event.preventDefault();
    //alert('A name was submitted: ' + this.state.value);
console.log(" this.state.value");
var apidata={
party_worker_type: this.state.worker_type,
age_range: this.state.age_range,
gender: this.state.gender,
taluk: this.state.taluk,
district:this.state.district,
panchayath_ward:this.state.ward_no,
pin:this.state.pincode,

}
  const api = `http://34.93.47.253:9004/mla_server/api/v1/partyworker_filter`
axios.post(api,apidata,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
            this.setState({ 'droplets': res.data.data });
}else{
        this.setState({'droplets':'' })

}
       
    })
  }
activatedata(id){

toastr.confirm('Are you sure confirm to activate the partyworker?',{
 okText:     <button className="alertok" onClick={() => this.activate(id)} >Ok</button>,
  buttons: [{
    okText: 'Do not apply',
    className: 'do-not-apply-btn',
    handler: () => console.log('do-not-apply clicked')
  }, {
    cancel: true // move the cancel button to the end
  }]

});
console.log("ssdd");

}
deactivatedata(id){

toastr.confirm('Are you sure confirm to deactivate the partyworker?',{
okText:     <button className="alertok" onClick={() => this.deactivate(id)} >Ok</button>,
  buttons: [{
    okText: 'Do not apply',
    className: 'do-not-apply-btn',
    handler: () => console.log('do-not-apply clicked')
  }, {
    cancel: true // move the cancel button to the end
  }]
  

});
console.log("ssdd");

}
activate(id){
console.log("ssdd",id);
const api = `http://34.93.47.253:9004/mla_server/api/v1/deactivate_partyworker`
axios.post(api,{
 party_worker_id: id,
    is_active:true
  }, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
this.props.history.push('/theme/colors');
       window.location.reload();
       
    })
}
deactivate(id){
console.log("ssdd",id);
const api = `http://34.93.47.253:9004/mla_server/api/v1/deactivate_partyworker`
axios.post(api,{
 party_worker_id: id,
    is_active:false
  }, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
this.props.history.push('/theme/colors');
       window.location.reload();

    })
}

getdistrict(){
this.district_list=[];
  const api = `http://34.93.47.253:9004/mla_server/api/v1/filter_category`
axios.get(api,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
        this.state.district_list = res.data.complaint_categories.district;
            console.log("this.district_list",this.state.district_list);

}else{


} 
 })

}
getdistrictdata(event){
            this.setState({ 'district': event.target.value});
	this.loadTaluk(event);

}
  loadTaluk(eventdata: any) {
    console.log("eventdata", eventdata.target.value);
console.log("this.district_list",this.state.district_list);
    var datalist = this.state.district_list.find(o => o.name == eventdata.target.value) || '';
    this.state.taluk_list = datalist.taluk;
    console.log("datalistssd", datalist)


    
  }

  loadPanchayat(eventdata: any){
            this.setState({ 'taluk': eventdata.target.value});
    var datalist = this.state.taluk_list.find(o => o.name == eventdata.target.value) || '';
    this.state.ward_list = datalist.panchayath_ward;
    console.log("this.state.ward_list", this.state.ward_list)
  }

wardevent(eventdata: any){
    console.log("eventdata", eventdata.target.value)
    var datalist = this.state.ward_list.find(o => o.name == eventdata.target.value) || '';
            this.setState({ 'ward': eventdata.target.value});
            this.setState({ 'pincode': datalist.pin});

}


edit(id){
console.log("id",id);
    localStorage.setItem('partyworkerid', id);
    const deptidvalue = localStorage.getItem('partyworkerid');
}
viewdata(id){

    localStorage.setItem('partyworkerid', id);
    const deptidvalue = localStorage.getItem('partyworkerid');

}
     render() {

        return (
           <div  >
            <form onSubmit={this.filtersubmit}>
<div className="row">
              <h1 id='title' className="floatleft">List of Party Workers</h1>

</div>
 <div className="form-group col-md-12">
          <h4> Filter Partyworker  </h4>
</div>
<div className="row">
                  <div className="form-group col-md-4">

<label>Worker Type</label>
<select className="slectbox"  className="width" value={this.state.worker_type}   name="worker_type"  placeholder="Choose showFirstLastButtons" onChange={this.filterevent}
                    >
                    <option value="" disabled selected>Choose Worker Type </option>
                    <option value='Youth Member'>Youth Member</option>
                    <option value='Women Member'>Women Member </option>
                    <option value='Panchayat/Ward Member'>Panchayat/Ward Member </option>


                  </select>
</div>

   <div className="form-group col-md-4">
<label>Select Gender</label>
<select className="slectbox"  className="width" placeholder="Choose showFirstLastButtons" value={this.state.gender}   name="gender"  onChange={this.filterevent}
                    >
                   <option value="" disabled selected>Choose Gender</option>
                    <option value="Male"> Male</option>
                    <option value="Female">Female </option>
                    <option value="Transgender">Transgender </option>


                  </select>

</div>



<div className="form-group col-md-4">
<label>Select Age Range</label>
<select className="slectbox"  className="width" placeholder="Choose showFirstLastButtons" value={this.state.age_range}   name="age_range"  onChange={this.filterevent}
                    >
                   <option value="" disabled selected>Choose Age Range </option>
                    <option value="18-25"> 18-25</option>
                    <option value="25-30">25-30 </option>
                    <option value="30-40">30-40 </option>


                  </select>
</div>
</div>
<div className="row">
<div className="form-group col-md-4">

<label>Select District</label>
<select className="slectbox"  className="width" placeholder="Choose showFirstLastButtons"  onChange={this.getdistrictdata} value={this.state.district}   name="district">
                    <option value="" disabled selected>Choose District </option>
                 {this.state.district_list.map(optn => (

                     <option>{optn.name}</option>
                 ))}
             </select>

</div>
<div className="form-group col-md-4">

<label>Select Taluk</label>
<select className="slectbox"  className="width" placeholder="Choose showFirstLastButtons"  onChange={this.loadPanchayat}
  value={this.state.taluk}   name="taluk"                   >
                   <option value="" disabled selected>Choose Taluk </option>
                   {this.state.taluk_list.map(optn => (

                     <option>{optn.name}</option>
                 ))}


                  </select>
</div>



<div className="form-group col-md-4">
<label>Select Ward</label>
<select className="slectbox"  className="width"  placeholder="Choose showFirstLastButtons" onChange={this.wardevent}
  value={this.state.panchayath_ward}   name="panchayath_ward"        
                    >
                   <option value="" disabled selected>Choose Ward </option>
                   {this.state.ward_list.map(optn => (

                     <option>{optn.name}</option>
                 ))}
                  </select>
</div>
</div>
<div className="row">
<div className="form-group col-md-4">
<label>Pincode</label>
<input type="text" value={this.state.pincode} name="pincode"  className="width" onChange={this.filterevent} />
</div>

</div>




                <button  type="submit" value="Submit" className="btn btn-primary">Submit</button>
              <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>

</form>
<Link to="/theme/addpartyworkers">
    <button className="btn btn-primary floatright">Add Party Worker</button>
</Link>
<div>
             <table className="table table-hover"  >
      <thead>
        <tr className="bgcolor">
          <th>Id</th>
          <th>Name</th>
          <th>Phone number</th>
          <th>Party worker type</th>
          <th>Taluk Person</th>
          <th>Ward</th>
          <th>District</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        { (this.state.droplets.length > 0) ? this.state.droplets.map( (droplet, index) => {
           return (
            <tr key={ index }>
              <td>{ index+1 }</td>
              <td>{ droplet.name }</td>
              <td>{ droplet.phone_number}</td>
              <td>{ droplet.party_worker_type }</td>
              <td>{ droplet.taluk }</td>
              <td>{ droplet.panchayath_ward }</td>
              <td>{ droplet.district }</td>
              <td><Link to="/theme/viewworker">
    <button className="btn btn-view" onClick={() => this.viewdata(droplet.party_worker_id)}>View</button>
</Link>

{droplet.is_active ? (
<div >
    <button className="btn btn-green" onClick={() => this.deactivatedata(droplet.party_worker_id)} >Deactivate</button>
</div>
) : <div >
    <button className="btn btn-green" onClick={() => this.activatedata(droplet.party_worker_id)} >Activate</button>
</div>}



<Link to="/partyworker/editpartyworker">
    <button className="btn btn-edit" onClick={() => this.edit(droplet.party_worker_id)}>Edit</button>
</Link></td>
            </tr>
          )
         }) : <tr><td colSpan="5">No records found</td></tr> }
      </tbody>
    </table>
</div>
           </div>
        )
     }
}
