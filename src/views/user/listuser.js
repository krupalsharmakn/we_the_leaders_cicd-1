import React, { Component } from "react";
import {withRouter, Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr';
//import Modal from 'react-modal';
import { Button,Modal } from 'react-bootstrap'

export  default class Listuser extends Component {
    constructor(props){
       super(props);
       this.state={
      droplets: [],
 showHide : false,
user_id:'',
notification_title:[],
notification_message:[],
status:'',
gender:'',
age:'',
marital_status:'',
blood_group:'',
pincode:'',
ward_no:'',
filtershow:false,
taluk_list:[],
ward_list:[],
district_list:[],
panchayath_ward:''
       }
    this.handleChange = this.handleChange.bind(this);
this.filterevent = this.filterevent.bind(this);
    this.filtersubmit = this.filtersubmit.bind(this);
this.getdistrictdata = this.getdistrictdata.bind(this);
    this.loadPanchayat = this.loadPanchayat.bind(this);
    this.wardevent = this.wardevent.bind(this);
   }

handleModalShowHide() {
        this.setState({ showHide: !this.state.showHide })
this.handleFormReset();
    }
FilterShowHide() {

        this.setState({ filtershow: !this.state.filtershow })
console.log("Ssdsddd",this.state);
    }

handleChange(event) {
const target = event.target;
console.log("event",event.target.value);
    const value = target.value;
    const name = target.name;
    this.setState({
[name]:value
    });;

}


componentDidMount(){
       console.log("sdjhfhsdf");
   const api = `http://35.193.187.77:9004/mla_server/api/v1/list_users`
axios.get(api, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
            this.setState({ 'droplets': res.data.data });  
    })
this.getdistrict();
  }

cancelclick(){
this.setState({
      status:'',
gender:'',
age:'',
marital_status:'',
blood_group:'',
pincode:'',
ward_no:'',
taluk:'',
district:'',
panchayath_ward:'',
       })
this.componentDidMount();
}
 filterevent(event) {
console.log("event",event.target);
const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });

console.log("state",this.state);
  }

     filtersubmit(event) {
event.preventDefault();
    //alert('A name was submitted: ' + this.state.value);
console.log(" this.state.value");
var apidata={
is_active: this.state.status,
age: this.state.age,
marital_status: this.state.marital_status,
pincode:  this.state.pincode,
blood_group:  this.state.blood_group,
gender:  this.state.gender,
ward_no:  this.state.ward_no,
district:this.state.district,
taluk:this.state.taluk,
panchayath_ward:this.state.panchayath_ward,

}
  const api = `http://35.193.187.77:9004/mla_server/api/v1/user_filter`
axios.post(api,apidata,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
            this.setState({ 'droplets': res.data.data });
}else{
        this.setState({'droplets':'' })

}
       
    })
  }

activatedata(id){

toastr.confirm('Are you sure confirm to activate the user?',{
 okText:     <button className="alertok" onClick={() => this.activate(id)} >Ok</button>,
  buttons: [{
    okText: 'Do not apply',
    className: 'do-not-apply-btn',
    handler: () => console.log('do-not-apply clicked')
  }, {
    cancel: true // move the cancel button to the end
  }]

});


}
deactivatedata(id){

toastr.confirm('Are you sure confirm to deactivate the user?',{
okText:     <button className="alertok" onClick={() => this.deactivate(id)} >Ok</button>,
  buttons: [{
    okText: 'Do not apply',
    className: 'do-not-apply-btn',
    handler: () => console.log('do-not-apply clicked')
  }, {
    cancel: true // move the cancel button to the end
  }]
  

});


}
viewdata(id){
console.log("id",id);
    localStorage.setItem('userid', id);
}
// Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)

sendnotify(user_id){
this.handleModalShowHide()
 this.setState({ 'user_id': user_id });  

// http://34.93.47.253:9004/mla_server/api/v1/send_message


}



sendmsg(){

this.handleModalShowHide();
this.handleFormReset();
var apidata={
to: "individual",
user_id: this.state.user_id,
notification_title: this.state.notification_title,
notification_message: this.state.notification_message
}

const api = `http://35.193.187.77:9004/mla_server/api/v1/send_message`
axios.post(api,apidata, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
toastr.success('Notification sent successfuly')
       
    })



}
activate(id){
console.log("ssdd",id);
const api = `http://35.193.187.77:9004/mla_server/api/v1/delete_user`
axios.post(api,{
 user_id: id,
    is_active:true
  }, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {
this.props.history.push('/userslist/listuser');
    window.location.reload();
       
    })

}

deactivate(id){
console.log("ssdd",id);
const api = `http://35.193.187.77:9004/mla_server/api/v1/delete_user`
axios.post(api,{
 user_id: id,
    is_active:false
  }, { headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'} })
        .then(res => {

       this.props.history.push('/userslist/listuser');
    window.location.reload();
    })

}

handleFormReset = () => {
    this.setState({
notification_title:'',
notification_message:'',
})
  }
getdistrict(){
this.district_list=[];
  const api = `http://35.193.187.77:9004/mla_server/api/v1/filter_category`
axios.get(api,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
        this.state.district_list = res.data.complaint_categories.district;
            console.log("this.district_list",this.state.district_list);

}else{


} 
 })

}
getdistrictdata(event){
            this.setState({ 'district': event.target.value});
	this.loadTaluk(event);

}
  loadTaluk(eventdata: any) {
    console.log("eventdata", eventdata.target.value);
console.log("this.district_list",this.state.district_list);
    var datalist = this.state.district_list.find(o => o.name == eventdata.target.value) || '';
    this.state.taluk_list = datalist.taluk;
    console.log("datalistssd", datalist)


    
  }

  loadPanchayat(eventdata: any){
            this.setState({ 'taluk': eventdata.target.value});
    var datalist = this.state.taluk_list.find(o => o.name == eventdata.target.value) || '';
    this.state.ward_list = datalist.panchayath_ward;
    console.log("this.state.ward_list", this.state.ward_list)
  }

wardevent(eventdata: any){
    console.log("eventdata", eventdata.target.value)
    var datalist = this.state.ward_list.find(o => o.name == eventdata.target.value) || '';
            this.setState({ 'panchayath_ward': eventdata.target.value});
            this.setState({ 'pincode': datalist.pin});

}

     render() {
        return (
 
           <div  >

<div className="row">
<div className="form-group col-md-12" >
              <h4 id='title' className="floatleft heading-color">List of Users</h4>
</div>
</div>
<div className="row">

     <div className="form-group col-md-12">
          <h5> Filter Users  </h5>

</div>
</div>
<form show={this.state.filtershow} onSubmit={this.filtersubmit}>
<div >
<div className="row">
                  <div className="form-group col-md-4">

<label>Select Status :</label>
<select className="slectbox"  className="width" value={this.state.status}   name="status"  placeholder="Choose showFirstLastButtons" onChange={this.filterevent}
                    >
                    <option value="" disabled selected>Choose Status </option>
                    <option value='true'>Active</option>
                    <option value='false'>Deactive </option>
                 
                  </select>
</div>

 <div className="form-group col-md-4">
<label>Select Gender</label>
<select className="slectbox"  className=" width" placeholder="Choose showFirstLastButtons" value={this.state.gender}   name="gender"  onChange={this.filterevent}
                    >
                   <option value="" disabled selected>Choose Gender</option>
                    <option value="Male"> Male</option>
                    <option value="Female">Female </option>
                    <option value="Other">Other </option>


                  </select>
</div>
<div className="form-group col-md-4">
<label>Age</label>
<input type="text" value={this.state.age} name="age"  className="width" onChange={this.filterevent} />


</div>
</div>
<div className="row">

<div className="form-group col-md-4">
<label>Select Marital Status</label>
<select className="slectbox"  className="width" placeholder="Choose showFirstLastButtons" value={this.state.marital_status}   name="marital_status"  onChange={this.filterevent}
                    >
                   <option value="" disabled selected>Choose Marital Status </option>
                    <option value="Married"> Married</option>
                    <option value="Unmarried">Unmarried </option>
                    <option value="Divorcee">Divorcee </option>


                  </select>
</div>


<div className="form-group col-md-4">
       <label> Blood Group</label>
<select className="slectbox"  className=" width" placeholder="Choose showFirstLastButtons" value={this.state.blood_group} name="blood_group" placeholder="Choose showFirstLastButtons" onChange={this.filterevent}
                    >
                    <option value="" disabled selected>Choose option </option>
                      <option value="A+" >A+</option>
                      <option value="A-" >A-</option>
                      <option value="B+">B+</option>
                      <option value="B-">B-</option>
                      <option value="AB+" >AB+</option>
                      <option value="AB-">AB-</option>
                      <option value="O+" >O+</option>
                      <option value="O-" >O-</option>
                  </select>
</div>

<div className="form-group col-md-4">

<label>Select District</label>
<select className="slectbox"  className="width" placeholder="Choose showFirstLastButtons"  onChange={this.getdistrictdata} value={this.state.district}   name="district">
                    <option value="" disabled selected>Choose District </option>
                 {this.state.district_list.map(optn => (

                     <option>{optn.name}</option>
                 ))}
             </select>

</div>
</div>

<div className="row">

<div className="form-group col-md-4">
<label>Select Taluk</label>
<select className="slectbox"  className=" width" placeholder="Choose showFirstLastButtons"  onChange={this.loadPanchayat}
  value={this.state.taluk}   name="taluk"                   >
                   <option value="" disabled selected>Choose Taluk </option>
                   {this.state.taluk_list.map(optn => (

                     <option>{optn.name}</option>
                 ))}


                  </select>
</div>

<div className="form-group col-md-4">
<label>Select Ward</label>
<select className="slectbox"  className="width"  placeholder="Choose showFirstLastButtons" onChange={this.wardevent}
  value={this.state.panchayath_ward}   name="panchayath_ward"        
                    >
                   <option value="" disabled selected>Choose Ward </option>
                   {this.state.ward_list.map(optn => (

                     <option>{optn.name}</option>
                 ))}
                  </select>
</div>

<div className="form-group col-md-4">
<label>Pincode</label>
<input type="text" value={this.state.pincode} name="pincode"  className="width" onChange={this.filterevent} readOnly/>
</div>
</div>
</div>


                <button  type="submit" value="Submit" className="btn btn-primary">Submit</button>
              <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>
</form>


<Link to="/userslist/adduser">
    <button className="btn btn-primary floatright">Add User</button>
</Link> 

  <div>
               

                <Modal show={this.state.showHide}>
                    <Modal.Header>

                    <Modal.Title>Hi</Modal.Title>

                    <Modal.Body>
<div   className="row" >
                <div className="form-group  col-md-6">
                    <label>Notification Title</label>
                    <input type="text" value={this.state.notification_title} name="notification_title" onChange={this.handleChange} className="width"  />
                </div>
</div> 
<div   className="row" >
                <div className="form-group  col-md-6">
                    <label>Notification Message</label>
                    <textarea type="text" value={this.state.notification_message} name="notification_message" onChange={this.handleChange} className="width"  />
                </div>
</div> 
</Modal.Body>
                    </Modal.Header>
                    <Modal.Footer>
                    <Button variant="secondary" onClick={() =>this.handleModalShowHide()}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={() =>this.sendmsg()}>
                        Send
                    </Button>
                    </Modal.Footer>
                </Modal>

            </div>
<div>
             <table className="table table-hover">
      <thead>
        <tr className="bgcolor">
          <th>Sl.No</th>
          <th>Name</th>
          <th>Email</th>
          <th>Mobile No</th>
          <th>Age</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        { (this.state.droplets.length > 0) ? this.state.droplets.map( (droplet, index) => {
           return (
            <tr key={ index }>
              <td>{ index+1 }</td>
              <td>{ droplet.first_name }</td>
              <td>{ droplet.email}</td>
              <td>{ droplet.mobile_number }</td>
              <td>{ droplet.age }</td>
              <td><Link to="/userslist/viewuser">
    <button className="btn btn-view" onClick={() => this.viewdata(droplet.user_id)}>View</button>
</Link>
 <Button  className="btn notify"  onClick={() => this.sendnotify(droplet.user_id)}>
                    Send notification
                </Button>

{droplet.is_active ? (
<div >
    <button className="btn btn-green" onClick={() => this.deactivatedata(droplet.user_id)} >Deactivate</button>
</div>
) : <div >
    <button className="btn btn-green" onClick={() => this.activatedata(droplet.user_id)} >Activate</button>
</div>}


</td>
            </tr>
          )
         }) : <tr><td colSpan="5">No records find</td></tr> }
      </tbody>
    </table>
</div>
           </div>
        )
     }
}
