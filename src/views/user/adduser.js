import React, { Component } from "react";
import { withRouter,Link } from "react-router-dom";
import axios from 'axios';
import {toastr} from 'react-redux-toastr'
const validEmailRegex = RegExp(
  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
);
const validateForm = errors => {
  let valid = true;
  Object.values(errors).forEach(val => val.length > 0 && (valid = false));
  return valid;
};
export  default class AddUser extends Component {
constructor(props){
      super(props);
    this.state = {
first_name: null,
last_name:'',
email:null,
mobile_number:null,
DOB:'',
gender:'',
taluk:'',
district:'',
panchayath_ward:'',
age:'',
age_range:'',
pincode:'',
address1:'',
address2:'',
address3:'',
blood_group:'',
adhaar_number:'',
father_name:'',
education:'',
image:'',
ward_no:'',
marital_status:'',
district_list:[],
taluk_list:[],
ward_list:[],
errors: {
        first_name: '',
        email: '',
mobile_number:'',
party_worker_type:''

      }

};
this.getdistrict();
     this.onChange = this.onChange.bind(this);
this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
this.fileChangedHandler = this.fileChangedHandler.bind(this);
this.getdistrictdata = this.getdistrictdata.bind(this);
    this.loadPanchayat = this.loadPanchayat.bind(this);
    this.wardevent = this.wardevent.bind(this);

   }
  onChange(e){
console.log("sdsddonchange");
     const re = /^[0-9\b]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
  this.handleChange(e)
        this.setState({value: e.target.value})
      }
 }

twoCalls = e => {
  this.onChange(e)
}

fileChangedHandler = (event) => {
  const file = event.target.files[0];
    this.setState({'image':event.target.files[0]});
console.log("this.state",this.state);
console.log("file",file)

}

     handleFormReset = () => {
 this.state = {
name: '',
email:'',
mobile_number:'',
alternative_number:'',
DOB:'',
gender:'',
taluk:'',
district:'',
age:'',
age_range:'',
pin:''};
    this.setState(() => this.state)
  }
   


cancelclick(){
 this.props.history.push('/userslist/listuser');
}
 handleChange(event) {
console.log("event",event.target);
const target = event.target;
  const { name, value } = event.target;

    let errors = this.state.errors;

    switch (name) {
      case 'name': 
        errors.first_name = 
          value.length < 1
            ? 'Enter user name!'
            : '';
        break;
     case 'email': 
        errors.email = 
          validEmailRegex.test(value)
            ? ''
            : 'Email is not valid!';
        break;
     case 'mobile_number': 
  errors.mobile_number = 
          value.length < 10
            ? 'Enter valid phone number!'
            : '';
        break;
      default:
        break;
    }

    this.setState({errors, [name]: value});
  }

  handleSubmit(event) {
    //alert('A name was submitted: ' + this.state.value);
event.preventDefault();
if(this.state.first_name!=null && this.state.email!=null && this.state.mobile_number!=null ){
 if(validateForm(this.state.errors)) {

 const formData = new FormData();
formData.append("first_name",this.state.first_name);
formData.append("last_name",this.state.last_name);
formData.append("mobile_number",this.state.mobile_number);
formData.append("email",this.state.email);
formData.append("dob",this.state.DOB);
formData.append("gender",this.state.gender);
formData.append("taluk",this.state.taluk);
formData.append("district",this.state.district);
formData.append("address1",this.state.address1);
formData.append("pincode",this.state.pincode);
formData.append("blood_group",this.state.blood_group);
formData.append("adhaar_number",this.state.adhaar_number);
formData.append("father_name",this.state.father_name);
formData.append("image",this.state.image);
formData.append("marital_status",this.state.marital_status);
formData.append("address2",this.state.address2);
formData.append("address3",this.state.address3);
formData.append("education",this.state.education);
formData.append("ward_no",this.state.ward_no);
formData.append("panchayath_ward",this.state.panchayath_ward);
formData.append("age",'');

  const api = `http://35.193.187.77:9004/mla_server/api/v1/add_user`
axios.post(api,formData,{ headers: {"Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
if(res.data.statuscode==200){
 this.props.history.push('/userslist/listuser');
toastr.success('Party worker added successfuly')
            console.log(res.data);
}
    })
}

}else{

console.log("invalid form",this.state);

 let errors = this.state.errors;
if(this.state.name==null || this.state.name==undefined ){
 errors.name ='Enter name ';
}
if(this.state.email==null || this.state.email==undefined ){
 errors.email ='Enter email id';
}
if(this.state.mobile_number==null || this.state.mobile_number==undefined ){
 errors.mobile_number ='Enter phone number';
}



this.setState({errors});
}
console.log(" this.state.value", this.state);

  }
getdistrict(){
  const api = `http://35.193.187.77:9004/mla_server/api/v1/filter_category`
axios.get(api,{ headers: {Authorization : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiYWRtaW4yMDIwIiwidXNlcl9pZCI6IjUzMTUyIn0sImlhdCI6MTU4OTUyNzkwM30.3TaRywt9fxBTjK69V-WWpAjuiBX3TkkyUJjnt4LNIvE",'Accept' : 'application/json',
  'Content-Type': 'application/json'}})
        .then(res => {
            console.log(res.data);
if(res.data.statuscode== 200){
        this.state.district_list = res.data.complaint_categories.district;
            console.log("this.district_list",this.state.district_list);

}else{


} 
 })

}
getdistrictdata(event){
            this.setState({ 'district': event.target.value});
	this.loadTaluk(event);

}
  loadTaluk(eventdata: any) {
    console.log("eventdata", eventdata.target.value);
console.log("this.district_list",this.state.district_list);
    var datalist = this.state.district_list.find(o => o.name == eventdata.target.value) || '';
    this.state.taluk_list = datalist.taluk;
    console.log("datalistssd", datalist)


    
  }

  loadPanchayat(eventdata: any){
            this.setState({ 'taluk': eventdata.target.value});
    var datalist = this.state.taluk_list.find(o => o.name == eventdata.target.value) || '';
    this.state.ward_list = datalist.panchayath_ward;
    console.log("this.state.ward_list", this.state.ward_list)
  }

wardevent(eventdata: any){
    console.log("eventdata", eventdata.target.value)
    var datalist = this.state.ward_list.find(o => o.name == eventdata.target.value) || '';
            this.setState({ 'panchayath_ward': eventdata.target.value});
            this.setState({ 'pincode': datalist.pin});

}
render() {
    const {errors} = this.state;
return (
           <form onSubmit={this.handleSubmit} noValidate>
<div   className="row" >
                <div className="form-group col-md-6">
                <h3 className="floatleft" id='title'>Add User</h3>
</div>
                <div className="form-group col-md-6">
                <Link className="nav-link floatright" to={"/userslist/listuser"}>Back</Link>
</div>
</div>
<div   className="row" >
                <div className="form-group col-md-6">
                    <label> First Name</label>
                    <input type="text" className="form-control" value={this.state.first_name} name="first_name" onChange={this.handleChange} maxlength="20" noValidate />
 {errors.first_name.length > 0 && 
                <span className='error'>{errors.first_name}</span>}
                </div>

    <div className="form-group col-md-6">
                    <label>Last Name</label>
                    <input type="text" className="form-control" value={this.state.last_name} name="last_name" onChange={this.handleChange} maxlength="10" noValidate />

                </div>


</div>
<div   className="row" >
                <div className="form-group col-md-6">
 <label>Phone number</label>
<input className="form-control"  value={this.state.mobile_number}   name="mobile_number" onChange={this.twoCalls} maxlength="10" noValidate />
{errors.mobile_number.length > 0 && 
                <span className='error'>{errors.mobile_number}</span>}

</div>
               <div className="form-group  col-md-6">
                    <label>Email Id</label>
                    <input type="email" value={this.state.email} name="email" className="form-control" onChange={this.handleChange} noValidate />
{errors.email.length > 0 && 
                <span className='error'>{errors.email}</span>}
                </div>
  
                
</div>


<div   className="row" >

                <div className="form-group  col-md-6">
                    <label> DOB</label>
<input type="date"  className="form-control" value={this.state.DOB} name="DOB"  onChange={this.handleChange} />

                </div>
           <div className="form-group  col-md-6">
                       <label> Gender</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons" value={this.state.gender} name="gender" placeholder="Choose showFirstLastButtons" onChange={this.handleChange}
                    >
                    <option value="" disabled selected>Choose option </option>
                    <option value="Male"> Male</option>
                    <option value="Female">Female </option>
                    <option value="Transgender">Transgender </option>


                  </select>

                </div>
                
</div>
<div   className="row" >

           
 <div className="form-group  col-md-6">
                    <label> Select Marital Status</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons" value={this.state.marital_status} name="marital_status" placeholder="Choose showFirstLastButtons" onChange={this.handleChange}
                    >
                    <option value="" disabled selected>Choose option </option>
                    <option value="Married"> Married</option>
                    <option value="UnMarried">UnMarried </option>
                    <option value="Other">Divorcee </option>


                  </select>

                </div>

 <div className="form-group  col-md-6">
                    <label> Aadhaar Number</label>
<input type="text"  className="form-control" value={this.state.adhaar_number} name="adhaar_number"  onChange={this.handleChange} maxlength="12" />

                </div>
       

</div>

<div  className="row">
<div className="form-group  col-md-6">
                    <label>Education Qualification </label>
<input type="text"  className="form-control" value={this.state.education} name="education"  onChange={this.handleChange} maxlength="10"/>

                </div>
<div className="form-group  col-md-6">
                    <label> Blood Group</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons" value={this.state.blood_group} name="blood_group" placeholder="Choose showFirstLastButtons" onChange={this.handleChange}
                    >
                    <option value="" disabled selected>Choose option </option>
 <option value="A+" >A+</option>
                      <option value="A-" >A-</option>
                      <option value="B+">B+</option>
                      <option value="B-">B-</option>
                      <option value="AB+" >AB+</option>
                      <option value="AB-">AB-</option>
                      <option value="O+" >O+</option>
                      <option value="O-" >O-</option>
                     
                  </select>
                </div>
 

 
</div>

<div  className="row">
<div className="form-group  col-md-6">
                    <label> Father/Husband Name</label>
<input type="text"  className="form-control" value={this.state.father_name} name="father_name"  onChange={this.handleChange} maxlength="20" />

                </div>
<div className="form-group  col-md-6">
                    <label> Address 1</label>
<textarea type="text"  placeholder="Building No./Flat no"  className="form-control" value={this.state.address1} name="address1"  onChange={this.handleChange} maxlength="150" />

                </div>
</div>

<div  className="row">
<div className="form-group  col-md-6">
                    <label> Address 2</label>
<textarea type="text"  placeholder="Street " maxlength="150" className="form-control" value={this.state.address2} name="address2"  onChange={this.handleChange} />

                </div>
<div className="form-group  col-md-6">
                    <label> Address 3</label>
<textarea type="text"  className="form-control" placeholder="Area " maxlength="200" value={this.state.address3} name="address3"  onChange={this.handleChange} />

                </div>
</div>

<div  className="row">

 <div className="form-group  col-md-6">
<label>Select District</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons"  onChange={this.getdistrictdata} value={this.state.district}   name="district">
                    <option value="" disabled selected>Choose District </option>
                 {this.state.district_list.map(optn => (

                     <option value={optn.name} >{optn.name}</option>
                 ))}
             </select>

</div>
<div className="form-group  col-md-6">
<label>Select Taluk</label>
<select className="slectbox"  className="form-control width" placeholder="Choose showFirstLastButtons"  onChange={this.loadPanchayat}
  value={this.state.taluk}   name="taluk"                   >
                   <option value="" disabled selected>Choose Taluk </option>
                   {this.state.taluk_list.map(optn => (

                     <option value={optn.name}>{optn.name}</option>
                 ))}


                  </select>
</div>
</div>
<div  className="row">

<div className="form-group  col-md-6">
<label>Select Ward</label>
<select className="slectbox"  className="form-control width"  placeholder="Choose showFirstLastButtons" onChange={this.wardevent}
  value={this.state.panchayath_ward}   name="panchayath_ward"        
                    >
                   <option value="" disabled selected>Choose Ward </option>
                   {this.state.ward_list.map(optn => (

                     <option value={optn.name} >{optn.name}</option>
                 ))}
                  </select>

</div>
<div className="form-group  col-md-6">
<label>PIN code</label>
  <input type="text" className="form-control" value={this.state.pincode} name="pincode"  onChange={this.twoCalls} maxlength="06"/>
</div>
    <div className="form-group  col-md-6">

<input className="image"type="file" onChange={this.fileChangedHandler}  />

</div>
</div>
                <button type="submit"  value="Submit" className="btn btn-primary">Submit</button>
                            <button  onClick={() => this.cancelclick()} className="btn cancelclr">Cancel</button>
                
            </form>
)

}
}
