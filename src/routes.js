import React from 'react';

const Toaster = React.lazy(() => import('./views/notifications/toaster/Toaster'));
const Tables = React.lazy(() => import('./views/base/tables/Tables'));

const Breadcrumbs = React.lazy(() => import('./views/base/breadcrumbs/Breadcrumbs'));
const Cards = React.lazy(() => import('./views/base/cards/Cards'));
const Carousels = React.lazy(() => import('./views/base/carousels/Carousels'));
const Collapses = React.lazy(() => import('./views/base/collapses/Collapses'));
const BasicForms = React.lazy(() => import('./views/base/forms/BasicForms'));

const Jumbotrons = React.lazy(() => import('./views/base/jumbotrons/Jumbotrons'));
const ListGroups = React.lazy(() => import('./views/base/list-groups/ListGroups'));
const Navbars = React.lazy(() => import('./views/base/navbars/Navbars'));
const Navs = React.lazy(() => import('./views/base/navs/Navs'));
const Paginations = React.lazy(() => import('./views/base/paginations/Pagnations'));
const Popovers = React.lazy(() => import('./views/base/popovers/Popovers'));
const ProgressBar = React.lazy(() => import('./views/base/progress-bar/ProgressBar'));
const Switches = React.lazy(() => import('./views/base/switches/Switches'));

const Tabs = React.lazy(() => import('./views/base/tabs/Tabs'));
const Tooltips = React.lazy(() => import('./views/base/tooltips/Tooltips'));
const BrandButtons = React.lazy(() => import('./views/buttons/brand-buttons/BrandButtons'));
const ButtonDropdowns = React.lazy(() => import('./views/buttons/button-dropdowns/ButtonDropdowns'));
const ButtonGroups = React.lazy(() => import('./views/buttons/button-groups/ButtonGroups'));
const Buttons = React.lazy(() => import('./views/buttons/buttons/Buttons'));
const Charts = React.lazy(() => import('./views/charts/Charts'));
const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const CoreUIIcons = React.lazy(() => import('./views/icons/coreui-icons/CoreUIIcons'));
const Flags = React.lazy(() => import('./views/icons/flags/Flags'));
const Brands = React.lazy(() => import('./views/icons/brands/Brands'));
const Alerts = React.lazy(() => import('./views/notifications/alerts/Alerts'));
const Badges = React.lazy(() => import('./views/notifications/badges/Badges'));
const Modals = React.lazy(() => import('./views/notifications/modals/Modals'));
const Colors = React.lazy(() => import('./views/theme/colors/Colors'));

const Typography = React.lazy(() => import('./views/theme/typography/Typography'));
const Widgets = React.lazy(() => import('./views/widgets/Widgets'));
const Users = React.lazy(() => import('./views/users/Users'));
const User = React.lazy(() => import('./views/users/User'));

const Partyworker = React.lazy(() => import('./views/theme/colors/partyworker'));
const AddPartyworker = React.lazy(() => import('./views/theme/addpartyworkers/addpartyworker'));
const ViewPartyworker = React.lazy(() => import('./views/theme/viewworker/viewpartyworker'));
const editPartyworker = React.lazy(() => import('./views/theme/editpartyworker/editPartyworker'));

// Department 

const Departmentlist = React.lazy(() => import('./views/Department/listDepartment'))
const Departmentadd = React.lazy(() => import('./views/Department/addDepartment'))
const Departmentview = React.lazy(() => import('./views/Department/viewDepartment'))
const Departmentedit = React.lazy(() => import('./views/Department/editDepartment'))

// Grievances 
const Grievanceslist = React.lazy(() => import('./views/grievances/listGrievances'))
const Grievancesview = React.lazy(() => import('./views/grievances/viewGrievances'))


// projects
const Projectslist = React.lazy(() => import('./views/projects/listProjects'))
const Projectsadd = React.lazy(() => import('./views/projects/addProject'))
const Projectsview = React.lazy(() => import('./views/projects/viewProject'))
const Projectsedit = React.lazy(() => import('./views/projects/editProject'))
//news
const Newslist = React.lazy(() => import('./views/news/listNews'))
const Newsadd = React.lazy(() => import('./views/news/addNews'))
const Newsview = React.lazy(() => import('./views/news/viewNews'))
const Newsedit = React.lazy(() => import('./views/news/editNews'))


// events
const Eventslist = React.lazy(() => import('./views/events/listEvents'))
const Eventsadd = React.lazy(() => import('./views/events/addEvent'))
const Eventsview = React.lazy(() => import('./views/events/viewEvent'))
const Eventsedit = React.lazy(() => import('./views/events/editEvent'))

// meeting requests

const Meetingrequestslist = React.lazy(() => import('./views/meeting_requests/listmeeting_requests'))
const ViewMeeting = React.lazy(() => import('./views/meeting_requests/viewmeeting'))
// users

const Userslist = React.lazy(() => import('./views/user/listuser'))
const Usersview = React.lazy(() => import('./views/user/viewuser'));
const Usersadd = React.lazy(() => import('./views/user/adduser'));
//profile
const Profileview = React.lazy(() => import('./views/profile/mlaprofile'))
const Profileedit = React.lazy(() => import('./views/profile/editprofile'))

const BannersImages = React.lazy(() => import('./views/Banner_Images/bannerimages'))



const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/theme', name: 'Theme', component: Colors, exact: true },
  { path: '/theme/colors', name: 'Partyworker', component: Partyworker },
  { path: '/theme/addpartyworkers', name: 'AddPartyworker', component: AddPartyworker },
  { path: '/partyworker/editpartyworker', name: 'EditPartyworker', component: editPartyworker },
  { path: '/theme/viewworker', name: 'ViewPartyworker', component: ViewPartyworker },
  { path: '/department/departmentlist', name: 'listdepartment', component: Departmentlist },
  { path: '/department/departmentadd', name: 'adddepartment', component: Departmentadd},
  { path: '/department/departmentview', name: 'viewdepartment', component: Departmentview },
  { path: '/department/departmentedit', name: 'editDepartment', component: Departmentedit },

  { path: '/grievances/listgrievances', name: 'listgrievance', component: Grievanceslist },
  { path: '/grievances/viewgrievances', name: 'viewgrievance', component: Grievancesview },

  
  { path: '/projects/projectslist', name: 'listdepartment', component: Projectslist },
  { path: '/projects/projectsadd', name: 'adddepartment', component: Projectsadd},
  { path: '/projects/projectsview', name: 'viewdepartment', component: Projectsview },
  { path: '/projects/projectsedit', name: 'viewdepartment', component: Projectsedit },

  
  { path: '/news/newslist', name: 'listnews', component: Newslist },
  { path: '/news/newsadd', name: 'addnews', component: Newsadd},
  { path: '/news/newsview', name: 'viewnews', component: Newsview },
  { path: '/news/newsedit', name: 'editnews', component: Newsedit},

  { path: '/events/eventslist', name: 'listevents', component: Eventslist },
  { path: '/events/eventsadd', name: 'addevents', component: Eventsadd},
  { path: '/events/eventsview', name: 'viewevents', component: Eventsview },
  { path: '/events/eventsedit', name: 'eventsedit', component: Eventsedit },

  { path: '/meeting_requests/listmeeting_requests', name: 'listmeetingrequests', component: Meetingrequestslist },

  { path: '/meeting_requests/ViewMeeting', name: 'ViewMeeting', component: ViewMeeting },
  
  { path: '/userslist/listuser', name: 'listuser', component: Userslist},
  { path: '/userslist/viewuser', name: 'Usersview', component: Usersview},
  { path: '/userslist/adduser', name: 'Usersadd', component: Usersadd},

  { path: '/mlaprofile/profile', name: 'Profileview', component: Profileview},
  { path: '/mlaprofile/editprofile', name: 'Profileedit', component: Profileedit},

  { path: '/bannerimages/bannerimages', name: 'bannerimages', component: BannersImages},

];

export default routes;

