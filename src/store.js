import { createStore,combineReducers } from 'redux'
import {reducer as toastrReducer} from 'react-redux-toastr'

const initialState = {
  sidebarShow: 'responsive'
}

const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case 'set':
      return {...state, ...rest }
    default:
      return state
  }
}

const reducers = {
  // ... other reducers ...
 changeState: changeState,
  toastr: toastrReducer // <- Mounted at toastr.
}
const reducer = combineReducers(reducers)

const store = createStore(reducer)
export default store
